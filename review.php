<?php
/*
Template Name: Review
*/

get_header(); ?>


    <div class="review row main" role="main">

        <article class="main-content small-12 columns medium-9   ">
            <?php while (have_posts()) : the_post(); ?>
                <div class="small-12 columns">
                <?php echo do_shortcode("[rm_review_pre_intro view=simple ]"); ?>

                <!-- INTRODUCTION -->
                <?php $introduction= get_post_meta(get_the_ID(),"r_casino_introduction",true);
                if (!empty($introduction)):
                    ?><header >
                    <h1><?php the_title(); ?></h1>
                    <p> <?php echo $introduction ?></p>
                    </header>
                    <?php
                endif
                ?>

                <!-- VIDEO/BANKING OPTIONS-->

                <br/>
                <!-- BEGIN post-intro -->

                <?php echo do_shortcode("[rm_review_post_intro ]"); ?>



                <!-- END post-intro -->
<br/>

                <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>


                <?php do_action('rm_page_before_comments'); ?>
                <?php comments_template(); ?>
                <?php do_action('rm_page_after_comments'); ?>
            <?php endwhile; ?>
                </div>
        </article>
        <aside id="left-sidebar" class=" columns small-12 medium-3 ">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>