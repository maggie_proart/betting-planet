<?php

/*
Template Name: Search Page
 */
get_header(); ?>
<?php
global $paged;

?>


    <div class="row main search" role="main">
    <div class="main-content small-12 columns medium-9" >
        <header>
           <h1 class="entry-title">Search Results for : <?php the_search_query(); ?> </h1>
        </header>



  <div class="columns">
        <?php while ( have_posts() ) : the_post(); ?>

        <article class="row collapse">



            <div class="small-12  columns">
                <h3><a href="<?php echo get_the_permalink()?>"><?php echo get_the_title()?></a></h3>
                <div class="description hide-for-small-only"> <?php echo wp_trim_words( wp_strip_all_tags(get_the_excerpt()), 25, '...' ) ?></div>
                <footer class="hide-for-small-only">
                    <?php rm_entry_meta(); ?>
                </footer>
            </div>
        </article>

        <?php

        endwhile;?>
        <br/>
        <?php if ( function_exists( 'rmc_pagination_ajax' ) ) { rmc_pagination_ajax(); } else if ( is_paged() ) { ?>
            <nav id="post-nav">
                <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
                <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
            </nav>
        <?php } ?>
    </div>
        </div>
        <aside class="columns small-12 medium-3 ">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>