
<?php
add_action('wp_head', 'author_meta_description', 1);

function author_meta_description(){
    $authordesc = get_the_author_meta( 'description' )?get_the_author_meta( 'description' ):get_bloginfo('description');
    echo '<meta name="description" content="'.$authordesc.'">';
}

get_header(); ?>


    <div class="row main news" role="main">
        <div class="main-content small-12 columns medium-9" >

            <header>
                <?php
                $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
                if($curauth->user_description) {?>
                    <div class="small-12 columns author-description">
                        <div class="row">
                            <div class="small-12 medium-2 columns"><?php echo get_avatar( get_the_author_meta( 'ID' ) , 120 ); ?> </div>
                            <div class="small-12 medium-10 columns"> <h1 class="entry-title"><?= $curauth->nickname; ?></h1><?= $curauth->user_description;?></div>
                        </div>
                    </div>

                <?php }
                else {?>
                    <h1 class="entry-title"><?=$curauth->nickname; ?></h1>
                <?php }
                ?>
            </header>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'content', 'archive' );?>
                <?php endwhile; ?>

            <?php else : ?>


            <?php endif; // End have_posts() check. ?>

            <?php /* Display navigation to next/previous pages when applicable */ ?>

            <?php if ( function_exists( 'rmc_pagination_ajax' ) ) { rmc_pagination_ajax(); } else if ( is_paged() ) { ?>
                <nav id="post-nav">
                    <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
                    <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
                </nav>
            <?php } ?>
            <footer>

                <?php wp_link_pages(); ?>


            </footer>
            <?php do_action( 'rm_page_before_comments' ); ?>
            <?php comments_template(); ?>
            <?php do_action( 'rm_page_after_comments' ); ?>

        </div>
        <aside class="columns small-12 medium-3">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>





<?php get_footer();
