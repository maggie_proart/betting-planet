<?php
/**
 * The template part for displaying article content
 *
 * @package WordPress
 * @subpackage RMC
 * @since RMC 1.0
 */
?>

<div class="archive-item row">
    <?php echo get_structured_data(['ID' => get_the_ID(), 'post_title'=>get_the_title(), 'post_content'=>get_the_excerpt(), 'post_excerpt'=>get_the_excerpt() ]); ?>

    <?php $class="small-12" ?>
    <?php if ( has_post_thumbnail() ) : ?>
        <a  class="small-4 medium-3 img-wrapper " href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php echo get_the_post_thumbnail(null,'medium'); ?>

        </a>
        <?php $class="small-8 medium-9" ?>
    <?php endif; ?>

    <div class="desc <?=$class?>">
        <a  class="" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <h3><?= get_the_title() ?></h3>
        <div>
            <time
                class="date"><?= get_the_date("j F", get_the_ID()) ?> <?= get_the_time("g:i a", get_the_ID()) ?> </time>
            <?php echo wp_trim_words( wp_strip_all_tags(get_the_excerpt()), 20, '...' ) ?>
        </div>
</a>
    </div>
</div>

