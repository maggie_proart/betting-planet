<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>


  <style>
      body {
          font-family: Arial, Tahoma, sans-serif;
          color:#ccc;
      }
      #pnf {
          margin-bottom:1rem;
      }
      h2 {
          color:#3c3c3c;
          font-size:3rem;
      }
      .search-submit {border:none;background: #2ab0f7;
          color: #fefefe;
          font-weight: 600;
          padding: .4rem 1rem;
          border-radius: 3px;
      text-decoration: none;
          display: inline-block;
      }
      .search-box {
          max-width:600px;
          margin:0 auto; position :relative; text-align:center;
      }
      .search-field {
          height: 1.6rem;
          border-radius: 3px;
          border: 1px solid #ccc;
          min-width: 200px;
          padding-left:10px;
      }
      img {max-width:100%;}
      .tv {
          margin-left:-30px;
      }
      .screen-reader-text {display:none;}
  </style>

<div id="pnf">
    <div class="search-box" >
        <?php
        if (get_theme_mod('rmc_logo')) :
            echo '<a href="/"><img alt="' . get_bloginfo('description') . '" src="' . esc_url(get_theme_mod('rmc_logo')) . '"></a>';
        else:
            ?><?php bloginfo('name'); ?>
            <?php
        endif;
        ?>
        <br/><br/>

        <img src="<?php echo get_template_directory_uri() ?>/assets/images/404_error_tv.png" alt="" title=""  class="tv"/>

        <h2>The page you are looking for cannot be found.</h2>
        <h3> Perhaps searching will help  </h3>
        <?php get_search_form(); ?>





    </div>
    <img src="<?php echo get_template_directory_uri() ?>/assets/images/404_error_bg.jpg" alt="" title="" style="width:100%; position:fixed; top:0; height:100%; z-index:-1 "/>

</div>

<?php get_footer(); ?>

