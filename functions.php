<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 2:12 PM
 */

require_once( 'library/cleanup.php' );
require_once( 'library/widget-area.php' );
require_once( 'library/theme-support.php' );
require_once( 'library/navigation.php');
require_once( 'library/enqueue-scripts.php');
require_once( 'library/entry-meta.php');
require_once( 'library/rmc.php');
require_once( 'library/metabox.php');
require_once( 'library/shortcodes/shortcodes.php');
require_once( 'library/structured-data.php');


/* widgets */
require_once( 'library/widgets/custom_sidebar_menu.php');
require_once( 'library/widgets/recent_posts.php');
require_once( 'library/widgets/social.php');
require_once( 'library/widgets/custom_menus.php');


if(is_plugin_active('rm-data-central/index.php')){

require_once( 'library/widgets/rm-central_tables.php');
    require_once( 'library/countries-list.php');
}
