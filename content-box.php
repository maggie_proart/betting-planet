<?php
/**
 * The template part for displaying article content
 *
 * @package WordPress
 * @subpackage RMC
 * @since RMC 1.0
 */
?>

<li class="small-12 medium-6 columns content-box"><a  href="<?= get_the_permalink() ?>">
        <div class="img-wrapper small-4 medium-12"><?php
            if (has_post_thumbnail()) {
                echo   get_the_post_thumbnail(get_the_ID(), "medium");
            }
            else {
                echo '<img alt="" title="'.get_the_title().'"  src="'.get_template_directory_uri() .'/assets/images/placeholder.jpg">';
            }

            ?></div>
        <div class="desc small-8 medium-12"> <h3>
                <?= wp_trim_words(wp_strip_all_tags(get_the_title()), 14, '...') ?>
            </h3>

        </div>
    </a>
</li>
