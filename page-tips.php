<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 */

get_header(); ?>
<?php
global $paged;

?>


    <div class="row main tips" role="main">
        <div class="main-content small-12  medium-9">
            <header>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <!--featured-->
            <?php if ($paged == 0) { ?>
                <?php
                //Get featured news
                $idObj = get_category_by_slug('free-picks');
                $id = $idObj->term_id;

                $atts = array(
                    'category' => $id,
                    'post_status' => 'publish',
                    'tag' => 'featured',
                    'posts_per_page' => 3,
                    'post_type' => array('post', 'page','rm_games'),
                );

                $posts_to_exclude = get_rm_featured_post($atts, true);


                ?>
            <?php } ?>
            <div class="row">
                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $atts['post__not_in'] = $posts_to_exclude;

                ?>

                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $posts_to_exclude= array_merge($posts_to_exclude,get_rm_featured_post($atts,true));
                $atts['post__not_in'] = $posts_to_exclude;

                ?>
                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $posts_to_exclude= array_merge($posts_to_exclude,get_rm_featured_post($atts,true));
                $atts['post__not_in'] = $posts_to_exclude;

                ?>
                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $posts_to_exclude= array_merge($posts_to_exclude,get_rm_featured_post($atts,true));
                ?>
            </div>
            <?php

            $args = array(
                'category_name' => 'free-picks',
                'post__not_in' => $posts_to_exclude,
                'post_status' => 'publish',
                'paged' => 1,


            );

            $the_query = new WP_Query($args);

            ?>

                <?php while ($the_query->have_posts()):
                    $the_query->the_post();
                    get_template_part('content', 'archive');

                endwhile; ?>



            <?php if (function_exists('rmc_pagination_ajax')) {
                rmc_pagination_ajax(null,25,http_build_query($args));
            } else if (is_paged()) { ?>
                <nav id="post-nav">
                    <div
                        class="post-previous"><?php next_posts_link(__('&larr; Older posts', 'foundationpress')); ?></div>
                    <div
                        class="post-next"><?php previous_posts_link(__('Newer posts &rarr;', 'foundationpress')); ?></div>
                </nav>
            <?php } ?>
        </div>
        <aside class="columns small-12 medium-3 ">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>