/*jslint node: true */
"use strict";


var gulp        = require('gulp');

var sass = require('gulp-sass');

// Enter URL of your local server here
// Example: 'http://localwebsite.dev'
var URL = '';
// Check for --production flag



var cssnano = require('gulp-cssnano');

gulp.task('nano', function() {
    return gulp.src('assets/css/big/rmc.css')
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css/_includes'));
});
// Clean task

gulp.task('sass', function () {
    return gulp.src('assets/scss/rmc.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/big'));
});

gulp.task('nano-promo', function() {
    return gulp.src('assets/css/big/promo_pre-intro.css')
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css/_includes'));
});
// Clean task

gulp.task('sass-promo', function () {
    return gulp.src('assets/scss/promo_pre-intro.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/big'));
});
gulp.task('nano-sbookmakers', function() {
    return gulp.src('assets/css/big/sbookmakers.css')
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css/_includes'));
});
// Clean task

gulp.task('sass-sbookmakers', function () {
    return gulp.src('assets/scss/sbookmakers.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/big'));
});
gulp.task('nano-a', function() {
    return gulp.src('assets/css/big/bookmakers-sidebar.css')
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css/_includes'));
});
// Clean task

gulp.task('sass-a', function () {
    return gulp.src('assets/scss/bookmakers-sidebar.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/big'));
});
gulp

gulp.task('nano-o', function() {
    return gulp.src('assets/css/big/odds.css')
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css/_includes'));
});
// Clean task

gulp.task('sass-o', function () {
    return gulp.src('assets/scss/odds.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/big'));
});
gulp.task('css', ['sass','nano']);
