<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 */

get_header(); ?>


    <div class="row main news" role="main">
    <div class="main-content small-12 columns medium-9" >
        <header>
            <h1 class="entry-title"><?php single_cat_title(); ?> <?php if ( $paged > 1 ) {?> page <?=$paged?> of Category archive.<?php }?></h1>
        </header>


        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'archive' );?>

        <?php

        endwhile;?>

        <?php if ( function_exists( 'rmc_pagination_ajax' ) ) { rmc_pagination_ajax(); } else if ( is_paged() ) { ?>
            <nav id="post-nav">
                <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
                <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
            </nav>
        <?php } ?>
    </div>
        <aside class="columns small-12 medium-3 ">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>