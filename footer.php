<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:12 PM
 */
?>

<footer class="footer">
    <div class="footer-top">
        <div class="row">
            <?php
            if (get_theme_mod('rmc_logo')) :
                echo '<div class="small-12 columns"><img alt="' . get_bloginfo('description') . '" src="' . esc_url(get_theme_mod('rmc_logo')) . '" ></div>';
            else:
                ?>
                <?php echo bloginfo('name') ?>
                <?php
            endif;
            ?><?php dynamic_sidebar( 'footer-top-widgets' ); ?>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="footer-widgets row ">
                <?php dynamic_sidebar( 'footer-bottom-widgets' ); ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>