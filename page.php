<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 */

get_header(); ?>


    <div class="row main" role="main">
        <div class="main-content small-12 columns medium-9" >
            <?php while ( have_posts() ) : the_post(); ?>
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>

                <?php the_content();?>

                <footer>

                    <?php wp_link_pages(); ?>


                </footer>
                <?php do_action( 'rm_page_before_comments' ); ?>
                <?php comments_template(); ?>
                <?php do_action( 'rm_page_after_comments' ); ?>
            <?php endwhile;?>
        </div>
        <aside class="columns small-12 medium-3">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
   </div>

<?php get_footer(); ?>