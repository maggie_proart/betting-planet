<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:11 PM
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <style class="text/css">

        <?php include (TEMPLATEPATH . '/assets/css/_includes/rmc.css');

         ?>

    </style>
    <script>(function (u) {
            function loadCSS(e, t, n) {
                "use strict";
                function r() {
                    for (var t, i = 0; i < d.length; i++)d[i].href && d[i].href.indexOf(e) > -1 && (t = !0);
                    t ? o.media = n || "all" : setTimeout(r)
                }

                var o = window.document.createElement("link"), i = t || window.document.getElementsByTagName("script")[0], d = window.document.styleSheets;
                return o.rel = "stylesheet", o.href = e, o.media = "only x", i.parentNode.insertBefore(o, i), r(), o
            }

            for (var i in u) {
                loadCSS(u[i]);
            }
        }(['https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800italic|Open+Sans+Condensed:300,300i,700', 'https://opensource.keycdn.com/fontawesome/4.5.0/font-awesome.min.css']));</script>
    <noscript>
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800italic|Open+Sans+Condensed:300,300i,700"
            rel="stylesheet" type="text/css">
    </noscript>
    <?php wp_head(); ?>
</head>

<body id="body" <?php body_class(); ?>  data-toggler="no-scroll">

<?php do_action('rmc_after_body'); ?>
<header class="header">
    <div class="row align-middle">
        <div class="medium-3 columns hide-for-small-only">
        <?php bp_get_social(); ?>
        </div>
        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home" class="logo-header small-6 medium-6 columns">
            <?php
            if (get_theme_mod('rmc_logo')) :
                echo '<img alt="' . get_bloginfo('description') . '" src="' . esc_url(get_theme_mod('rmc_logo')) . '">';
            else:
                ?><?php bloginfo('name'); ?>
                <?php
            endif;
            ?>
        </a>

        <div class="small-6 medium-3 columns text-right">

            <button id="mobile-icon" class="menu-icon hide-for-large" type="button"
                    data-toggle="mobile-menu mobile-icon body" data-toggler="light"
                    aria-expanded="false"></button>
            <div class="search show-for-large">
                <form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">

                    <input type="text" value="" name="s" id="s" placeholder="search">
                    <button type="submit" id="search-submit">+</button>
                </form>
            </div>
        </div>
    </div>


</header>
<div class="desktop-menu-wrapper">
<div class="row show-for-large">
    <?php rm_main_nav(); ?>
</div>
</div>
<nav class="vertical menu hide hide-for-large" id="mobile-menu" role="navigation" data-toggler="hide">

    <form role="search" method="get" id="search-mobile" class="search-mobile" action="<?php echo home_url('/'); ?>">
        <input type="text" value="" name="s" id="s" placeholder="Search">
        <!--<button type="submit" id="search-submit"><i class="fa fa-search" aria-hidden="true"></i></button>-->
    </form>
    <?php rm_mobile_nav(); ?>
</nav>

<?php if (get_theme_mod('banner_show_setting')) { ?>
    <div class="banner ">
    <div class="row">
        <div class="small-12 columns text-center"><a title="<?php echo get_theme_mod('banner_link_title_setting') ?>" href="<?php echo get_theme_mod('banner_link_setting') ?>" target="_blank">
                <?php
                echo '<img src="' . get_theme_mod('rmb_site_banner') . '" title="Free Bonus bet" alt="'.get_theme_mod('banner_alt_setting').'" class="hide-for-small-only"/>';
                echo '<img src="' . get_theme_mod('rmb_site_banner_mobile') . '" title="Free Bonus bet"  alt="'.get_theme_mod('banner_alt_setting').'" class="show-for-small-only"/>';
                ?>
            </a></div>
    </div>
    </div><?php
} ?>

