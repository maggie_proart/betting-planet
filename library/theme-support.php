<?php
/**
 * Register theme support for languages, menus, post-thumbnails, post-formats etc.
 *
 * @package rmc
 * @since rmc 1.0.0
 */
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
function wpcodex_add_excerpt_support_for_pages() {
	add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );

if ( ! function_exists( 'rmc_theme_support' ) ) :
function rmc_theme_support() {

	add_image_size( 'med-rmc', 420, 220 );

	// Add language support
	load_theme_textdomain( 'rmc', get_template_directory() . '/languages' );

    // Image tiny_thumb
	add_image_size( 'tiny_thumb', 72, 50, array( 'center', 'center' ) );

	// Switch default core markup for search form, comment form, and comments to output valid HTML5
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add menu support
	add_theme_support( 'menus' );

	// Let WordPress manage the document title
	add_theme_support( 'title-tag' );

	/*add_filter('document_title_parts', 'dq_override_post_title', 10);
	function dq_override_post_title($title){
		global $paged;
		// change title for singular blog post

			// change title parts here

			$title['page'] = $paged; // optional



		return $title;
	}*/
	// Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
	add_theme_support( 'post-thumbnails' );

	// RSS thingy
	add_theme_support( 'automatic-feed-links' );

	// Add post formarts support: http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );

	// Declare WooCommerce support per http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
	//add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'rmc_theme_support' );
endif;


// Add logos instead of website title
function rmc_theme_customizer( $wp_customize ) {

	/*************************/
	/*                       */
	/*  1. Social Section    */
	/*                       */
	/*************************/


	$wp_customize->add_section( 'bp_social_section' , array(
		'title'    => __( 'Social', 'bp' ),
		'priority' => 30
	) );

	$wp_customize->add_setting('bp-facebook');
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'bp-facebook-link',
			array(
				'label'          => __( 'Facebook Link', 'rmb' ),
				'section'        => 'bp_social_section',
				'settings'       => 'bp-facebook',
				'type'           => 'text',

			)
		)
	);
	$wp_customize->add_setting('bp-twitter');
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'bp-twitter-link',
			array(
				'label'          => __( 'Twitter Link', 'rmb' ),
				'section'        => 'bp_social_section',
				'settings'       => 'bp-twitter',
				'type'           => 'text',
				'description' => 'Twitter'

			)
		)
	);
	$wp_customize->add_setting('bp-google');
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'bp-google-link',
			array(
				'label'          => __( 'Google+ Link', 'rmb' ),
				'section'        => 'bp_social_section',
				'settings'       => 'bp-google',
				'type'           => 'text',

			)
		)
	);
	//TAGLINE
	// add a setting for the site logo
	$wp_customize->add_setting('og_image');
// Add a control to upload the logo
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'og_image',
		array(
			'label' => 'Open Graph Image',
			'section' => 'title_tagline',
			'settings' => 'og_image',
			'description' => 'Upload a default Open Graph image'
		) ) );
	//description
	$wp_customize->add_setting( 'og_keywords' , array() );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'og_keywords',
			array(
				'label'          => __( 'Open Graph Keywords', 'rmb' ),
				'section'        => 'title_tagline',
				'settings'       => 'og_keywords',
				'type'           => 'text',

			)
		)
	);

	$wp_customize->add_setting( 'og_description' , array() );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'og_description',
			array(
				'label'          => __( 'Open Graph Description', 'rmb' ),
				'section'        => 'title_tagline',
				'settings'       => 'og_description',
				'type'           => 'textarea',

			)
		)
	);

 /* header */
	$wp_customize->add_section( 'rmc_header_section' , array(
		'title'       => __( 'Header', 'rmb' ),
		'priority'    => 30,
		'description' => 'Header type : Menu next to logo/ Full width Menu',
	) );
	$wp_customize->add_setting( 'rmc_header', array(
		'default'     => 'side',
		'transport'   => 'refresh',
	) );
	$wp_customize->add_control(
		'your_control_id',
		array(
			'label'    => __( 'Header Type', 'rmb' ),
			'section'  => 'rmc_header_section',
			'settings' => 'rmc_header',
			'type'     => 'radio',
			'choices'  => array(
				'full'  => 'Menu full width',
				'side' => 'Menu next to logo',
			),
		)
	);

 /* Logo*/
	$wp_customize->add_section( 'rmc_logo_section' , array(
		'title'       => __( 'Logo', 'rmc' ),
		'priority'    => 30,
		'description' => 'Upload a logo',
	) );
	$wp_customize->add_setting( 'rmc_logo' );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'rmc_logo', array(
		'label'    => __( 'Logo', 'rmc' ),
		'section'  => 'rmc_logo_section',
		'settings' => 'rmc_logo',
	) ) );


	/*********/
	/* BANNER*/
	/*********/
	$wp_customize->add_section( 'rmb_section_banner' , array(
		'title'    => __( 'Banner', 'rmb' ),
		'priority' => 30
	) );

    /* Show Banner */
	$wp_customize->add_setting( 'banner_show_setting' , array(
    'default'=>false
	) );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'banner-show',
			array(
				'label'          => __( 'Show banner ?', 'rmb' ),
				'section'        => 'rmb_section_banner',
				'settings'       => 'banner_show_setting',
				'type'           => 'checkbox',

			)
		)
	);

	$wp_customize->add_setting( 'banner_show_moolah' , array(
		'default'=>false
	) );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'banner-show-moolah',
			array(
				'label'          => __( 'Show Moolah ticker ?', 'rmb' ),
				'section'        => 'rmb_section_banner',
				'settings'       => 'banner_show_moolah',
				'type'           => 'checkbox',

			)
		)
	);

	/* Background Banner */
	$wp_customize->add_setting( 'banner_bg_setting' , array(
		'default'     => '#000000',
		'transport'   => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'banner_bg', array(
		'label'        => __( 'Banner Background', 'rmb' ),
		'section'    => 'rmb_section_banner',
		'settings'   => 'banner_bg_setting',
	) ) );
	$wp_customize->add_setting( 'rmb_site_banner' , array(

		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'banner_img', array(
		'label'    => __( 'Banner', 'starter' ),
		'section'  => 'rmb_section_banner',
		'settings' => 'rmb_site_banner',
	) ) );

	$wp_customize->add_setting( 'rmb_site_banner_mobile' , array(

		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'banner_img_mobile', array(
		'label'    => __( 'Banner Mobile' , 'starter' ),
		'section'  => 'rmb_section_banner',
		'settings' => 'rmb_site_banner_mobile',
	) ) );
	$wp_customize->add_setting( 'banner_link_setting' , array() );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'banner-link',
			array(
				'label'          => __( 'Banner Link', 'rmb' ),
				'section'        => 'rmb_section_banner',
				'settings'       => 'banner_link_setting',
				'type'           => 'text',

			)
		)
	);
	$wp_customize->add_setting( 'banner_link_title_setting' , array() );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'rm_banner_link_title', array(
		'label'        => __( '  ', 'rmb' ),
		'description' => '<h3>banner Link Title</h3>',
		'section'  => 'rmb_section_banner',
		'settings' => 'banner_link_title_setting',
	) ) );
	$wp_customize->add_setting( 'banner_alt_setting' , array() );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'rm_banner_alt', array(
		'label'        => __( '  ', 'rmb' ),
		'description' => '<h3>banner alt</h3>',
		'section'  => 'rmb_section_banner',
		'settings' => 'banner_alt_setting',
	) ) );
	$wp_customize->add_setting( 'top_banner_img_setting' , array() );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'top_banner_img', array(
		'label'        => __( '  ', 'rmb' ),
		'description' => '<hr><h2>Top bar banner</h2><br/><h3>banner image</h3>',
		'section'  => 'rmb_section_banner',
		'settings' => 'top_banner_img_setting',
	) ) );
	$wp_customize->add_setting( 'top_banner_link_setting' , array() );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'top_banner-link',
			array(
				'label'          => __( 'Top Banner Link', 'rmb' ),
				'section'        => 'rmb_section_banner',
				'settings'       => 'top_banner_link_setting',
				'type'           => 'text',

			)
		)
	);

	$wp_customize->add_setting( 'top_banner_link_setting' , array() );
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'top_banner-link',
			array(
				'label'          => __( 'Top Banner Link', 'rmb' ),
				'section'        => 'rmb_section_banner',
				'settings'       => 'top_banner_link_setting',
				'type'           => 'text',

			)
		)
	);

	/*Highlights */
	$wp_customize->add_section( 'rmb_section_highlight' , array(
		'title'    => __( 'Theme colors', 'rmb' ),
		'priority' => 30
	) );
	$wp_customize->add_setting( 'highlights_setting' , array(
		'default'     => '#dddddd',
		'transport'   => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'highlight_color', array(
		'label'        => __( 'Highlights Color', 'rmb' ),
		
		'section'    => 'rmb_section_highlight',
		'settings'   => 'highlights_setting',
	) ) );

	$wp_customize->add_setting( 'topabr_bg_setting' , array(
		'default'     => '#cccccc',
		'transport'   => 'refresh',

	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_bg_color', array(
		'label'        => __( '<hr>Top bar', 'rmb' ),

		'section'    => 'rmb_section_highlight',
		'settings'   => 'topabr_bg_setting',
		'description' => 'Please choose top bar background and border colours<br/><h3>background color</h3>'
	) ) );
	$wp_customize->add_setting( 'topabr_border_setting' , array(

		'transport'   => 'refresh',

	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_border_color', array(
		'label'        => __( 'Border color', 'rmb' ),

		'section'    => 'rmb_section_highlight',
		'settings'   => 'topabr_border_setting',
	) ) );

	/* menu */

	$wp_customize->add_setting( 'topabr_menu_setting' , array(

		'transport'   => 'refresh',
		'description' => 'This is a settings section.'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_background_color', array(
		'label'        => __( '<hr> Menu ', 'rmb' ),
		'description' => 'Please choose top bar menu colours<br/><h3>background colour</h3>',
		'section'    => 'rmb_section_highlight',
		'settings'   => 'topabr_menu_setting',
	) ) );
  /* side bar */
	$wp_customize->add_setting( 'sidebar_title_setting' , array(

		'transport'   => 'refresh',

	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_title_color', array(
		'label'        => __( '<hr> Sidebar ', 'rmb' ),
		'description' => 'Please choose sidebar  colours<br/><h3>title colour</h3>',
		'section'    => 'rmb_section_highlight',
		'settings'   => 'sidebar_title_setting',
	) ) );

	$wp_customize->add_setting( 'sidebar_bg_setting' , array(

		'transport'   => 'refresh',

	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_bg_color', array(
		'label'        => __( 'background color ', 'rmb' ),
		'section'    => 'rmb_section_highlight',
		'settings'   => 'sidebar_title_setting',
	) ) );


}
add_action( 'customize_register', 'rmc_theme_customizer' );




/** Add Custom Field To Category Form */
add_action( 'category_add_form_fields', 'category_form_custom_field_add', 10 );
add_action( 'category_edit_form_fields', 'category_form_custom_field_edit', 10, 2 );

function category_form_custom_field_add( $taxonomy ) {
	?>
	<div class="form-field">
		<label for="category_color">Category Color</label>
		<input name="category_color" id="category_custom_order" type="text" value="" size="40" aria-required="true" />
		<p class="description">Enter a Label color.</p>
	</div>
	<?php
}

function category_form_custom_field_edit( $tag, $taxonomy ) {

	$option_name = 'category_color_' . $tag->term_id;
	$category_color = get_option( $option_name );

	?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="category_color">Category Color</label></th>
		<td>
			<input type="text" name="category_color" id="category_color" value="<?php echo esc_attr( $category_color ) ? esc_attr( $category_color ) : ''; ?>" size="40" aria-required="true" />
			<p class="description">Enter a label color.</p>
		</td>
	</tr>
	<?php
}

/** Save Custom Field Of Category Form */
add_action( 'created_category', 'category_form_custom_field_save', 10, 2 );
add_action( 'edited_category', 'category_form_custom_field_save', 10, 2 );

function category_form_custom_field_save( $term_id, $tt_id ) {

	if ( isset( $_POST['category_color'] ) ) {
		$option_name = 'category_color_' . $term_id;
		update_option( $option_name, $_POST['category_color'] );
	}
}

function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
}
add_action( 'init', 'add_taxonomies_to_pages' );

function darken_color($rgb, $darker=2) {

	$hash = (strpos($rgb, '#') !== false) ? '#' : '';
	$rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
	if(strlen($rgb) != 6) return $hash.'000000';
	$darker = ($darker > 1) ? $darker : 1;

	list($R16,$G16,$B16) = str_split($rgb,2);

	$R = sprintf("%02X", floor(hexdec($R16)/$darker));
	$G = sprintf("%02X", floor(hexdec($G16)/$darker));
	$B = sprintf("%02X", floor(hexdec($B16)/$darker));

	return $hash.$R.$G.$B;
}