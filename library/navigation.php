<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 2/09/2016
 * Time: 9:53 AM
 */

register_nav_menus(array(
    'main-nav'  => 'Main Menu Desktop',
    'mobile-nav' => 'Mobile Menu',
));

if ( ! function_exists( 'rm_main_nav' ) ) {
    function rm_main_nav() {
        wp_nav_menu( array(
            'container'      => false,
            'menu_class'     => 'dropdown menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s desktop-menu hide-for-small-only" >%3$s</ul>',
            'theme_location' => 'main-nav',
            'depth'          => 3,
            'fallback_cb'    => false,

        ));
    }
}
if ( ! function_exists( 'rm_mobile_nav' ) ) {
    function rm_mobile_nav()
    {

        wp_nav_menu( array(
            'theme_location'  => 'mobile-nav',
            'container'       => false,
            'menu_class'      => 'menu vertical ',
            'items_wrap'      => '<ul id="menu-mobile" >%3$s</ul>',
            'depth'           => 0,
            'walker' => new dc_walker
        ));

    }
}


class dc_walker extends Walker_Nav_Menu {

    /*
	 * Add vertical menu class and submenu data attribute to sub menus
	 */

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $a='';
        if ($depth==0){
            $a= "<i class='fa fa-angle-down'></i><input type='checkbox'>";
        }
        $output .= $a."\n$indent<ul class=\"submenu mega menu vertical\" data-submenu>\n";
    }
}

//Optional fallback
function f6_topbar_menu_fallback($args)
{
    /*
     * Instantiate new Page Walker class instead of applying a filter to the
     * "wp_page_menu" function in the event there are multiple active menus in theme.
     */

    $walker_page = new Walker_Page();
    $fallback = $walker_page->walk(get_pages(), 0);
    $fallback = str_replace("<ul class='children'>", '<ul class="children submenu menu vertical" data-submenu>', $fallback);

    echo '<ul class="menu vertical large-horizontal" data-responsive-menu">'.$fallback.'</ul>';
}

