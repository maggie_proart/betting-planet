/**
 * Created by Maggie on 20/01/2017.
 */

(function() {
    function replaceLineBreaks(data) {
        var replacedData = data.replace("\r \n", "<br>");
        return replacedData;
    }
    tinymce.PluginManager.add('accordion', function( editor, url ) {
        editor.addButton( 'accordion', {
            id: 'rm_accordion',
            text: 'RM Accordion',
            icon: false,
            onclick: accordionDialog,
        });
        function accordionDialog() {

            editor.windowManager.open({
                title: 'Accordion',
                url: url + '/accordion.html',
                width: 800,
                height: 600,
                buttons: [
                     {
                        text:'insert accordion',onclick: function(e) {
                         var frame = jQuery(e.currentTarget).find("iframe").get(0);
                         var content = frame.contentDocument;
                         jQuery(content).find(".brmc-accordion .delete-me").remove();
                         var accordion = jQuery(content).find(".brmc-accordion");
                         console.log(jQuery('<div>').append(accordion.clone()).html());
                         editor.insertContent( jQuery('<div>').append(accordion.clone()).html());
this.parent().parent().close();



                     }

                    },
                    {text: 'Close', onclick: 'close'}
                ]
            });

        }

    });
})();