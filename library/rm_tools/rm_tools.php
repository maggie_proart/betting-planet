<?php

add_action('admin_head', 'rm_add_my_tc_button');

function rm_add_my_tc_button() {
    global $typenow;
    // check user permissions
    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
        return;
    }
    // verify the post type
    if( ! in_array( $typenow, array( 'post', 'page' ) ) )
        return;
    // check if WYSIWYG is enabled
    if ( get_user_option('rich_editing') == 'true') {
        add_filter("mce_external_plugins", "rm_add_tinymce_plugin");
        add_filter('mce_buttons', 'rm_register_my_tc_button');
    }
}
function rm_add_tinymce_plugin($plugin_array) {
    $plugin_array['table'] = get_template_directory_uri()."/library/rm_tools/js/table/plugin.js";
    $plugin_array['visualblocks'] = get_template_directory_uri()."/library/rm_tools/js/visualblocks/plugin.js";
    $plugin_array['accordion'] = get_template_directory_uri()."/library/rm_tools/js/accordion/plugin.js";

    return $plugin_array;
}

function rm_register_my_tc_button($buttons) {
    array_push($buttons, 'table','visualblocks','accordion');
    return $buttons;
}

function gavickpro_tc_css() {
    wp_enqueue_style('rm-tc', get_template_directory_uri()."/library/rm_tools/css/rm_tools.css");
}

add_action('admin_enqueue_scripts', 'gavickpro_tc_css');


?>