<?php


function rm_social_register_widgets() {
    register_widget( 'rm_social_media');
}
add_action( 'widgets_init', 'rm_social_register_widgets' );

class rm_social_media extends WP_Widget {

    function __construct() {
        parent::__construct(
            'rm_social_media', // Base ID
            __('RaceMedia Social ', 'text_domain'), // Name
            array('description' => __('Widget Social', 'text_domain'),) // Args
        );
    }



    public function widget( $args, $instance ) {

        extract($args);

        $title = apply_filters('widget_title', empty($instance['title']) ? __('Social') : $instance['title'], $instance, $this->id_base);
        /** This filter is documented in wp-includes/default-widgets.php */
        $instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        $before_widget = $args['before_widget'];
        if( strpos($args['before_widget'], 'class') === false ) {
            $before_widget = str_replace('>', 'class="'. $instance['extra-class'] . '"', $before_widget);
        }
        // there is 'class' attribute - append width value to it
        else {
            $before_widget = str_replace('class="', 'class="'. $instance['extra-class'] . ' ', $before_widget);
        }

        echo $before_widget;

        if ( !empty($instance['title']) )
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
        echo
            '<div class="social-list-wrapper "> <ul class="social-list">
			<li>
				<a href="' . $instance['facebook'] . '"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			</li>

			<li>
				<a href="' . $instance['twitter'] . '"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			</li>

			<li>
				<a href="' . $instance['google'] . '"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
			</li>

		</ul></div>';
        echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['facebook'] = strip_tags($new_instance['facebook']);
        $instance['twitter'] = strip_tags($new_instance['twitter']);
        $instance['google'] = strip_tags($new_instance['google']);
        $instance['extra-class'] = strip_tags($new_instance['extra-class']);
        return $instance;
    }

    // Widget form creation
    public function form($instance) {
        $title='';
        $facebook = '';
        $twitter = '';
        $google='';
        $extra_class='';

        // Check values
        if( $instance) {
            $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
            $facebook = esc_attr($instance['facebook']);
            $twitter = esc_textarea($instance['twitter']);
            $google = esc_textarea($instance['google']);
            $extra_class = esc_textarea($instance['extra-class']);
        } ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>
        <p>
            <label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e('Facebook Url', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" type="text" value="<?php echo $facebook; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e('Twitter Url:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="text" value="<?php echo $twitter; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('google'); ?>"><?php _e('Google+ Url:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('google'); ?>" name="<?php echo $this->get_field_name('google'); ?>" type="text" value="<?php echo $google; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('extra-class'); ?>"><?php _e('Css class:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('extra-class'); ?>" name="<?php echo $this->get_field_name('extra-class'); ?>" type="text" value="<?php echo $extra_class; ?>" />
        </p>
    <?php }
}