<?php
/**
 * Navigation Menu widget class
 *
 * @since 3.0.0
 */
class rm_multiple_menu_Widget extends WP_Widget {

    function __construct() {

        parent::__construct(
            'rm_multiple_menu_widget', // Base ID
            __( 'RaceMedia Multiple menu Menu', 'text_domain' ), // Name
            array( 'description' => __( 'add a multiple custom menu Widget', 'text_domain' ), ) // Args
        );
    }

    function widget($args, $instance) {
        // Get menu
        $menu_1 = ! empty( $instance['menu_1'] ) ? wp_get_nav_menu_object( $instance['menu_1'] ) : false;
        $menu_2 = ! empty( $instance['menu_2'] ) ? wp_get_nav_menu_object( $instance['menu_2'] ) : false;
        $menu_3 = ! empty( $instance['menu_3'] ) ? wp_get_nav_menu_object( $instance['menu_3'] ) : false;


        if ( !$menu_1 )
            return;

        /** This filter is documented in wp-includes/default-widgets.php */
        $instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];

        if ( !empty($instance['title']) )
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
        echo '<div class="rm-menus row">';
        wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $menu_1,'container_class'=>'small-12 medium-4 columns' ) );
        wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $menu_2,'container_class'=>'small-12 medium-4 columns' ) );
        wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $menu_3,'container_class'=>'small-12 medium-4 columns' ) );


        echo '<div class="clear"></div></div>';
        echo $args['after_widget'];
    }

    function update( $new_instance, $old_instance ) {
        $instance['title'] = strip_tags( stripslashes($new_instance['title']) );
        $instance['menu_1'] = (int) $new_instance['menu_1'];
        $instance['menu_2'] = (int) $new_instance['menu_2'];
        $instance['menu_3'] = (int) $new_instance['menu_3'];

        return $instance;
    }

    function form( $instance ) {
        $title = isset( $instance['title'] ) ? $instance['title'] : '';
        $menu_1 = isset( $instance['menu_1'] ) ? $instance['menu_1'] : '';
        $menu_2 = isset( $instance['menu_2'] ) ? $instance['menu_2'] : '';
        $menu_3 = isset( $instance['menu_3'] ) ? $instance['menu_3'] : '';



        // Get menus
        $menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );

        // If no menus exists, direct the user to go and create some.
        if ( !$menus ) {
            echo '<p>'. sprintf( __('No menus have been created yet. <a href="%s">Create some</a>.'), admin_url('nav-menus.php') ) .'</p>';
            return;
        }

        ?>
        <p>

            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('menu_1'); ?>"><?php _e('Select Menu:'); ?></label>
            <select id="<?php echo $this->get_field_id('menu_1'); ?>" name="<?php echo $this->get_field_name('menu_1'); ?>">
                <option value="0"><?php _e( '&mdash; Select &mdash;' ) ?></option>
                <?php
                foreach ( $menus as $menu ) {
                    echo '<option value="' . $menu->term_id . '"'
                        . selected( $menu_1, $menu->term_id, false )
                        . '>'. esc_html( $menu->name ) . '</option>';
                }
                ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('menu_2'); ?>"><?php _e('Select Menu:'); ?></label>
            <select id="<?php echo $this->get_field_id('menu_2'); ?>" name="<?php echo $this->get_field_name('menu_2'); ?>">
                <option value="0"><?php _e( '&mdash; Select &mdash;' ) ?></option>
                <?php
                foreach ( $menus as $menu ) {
                    echo '<option value="' . $menu->term_id . '"'
                        . selected( $menu_2, $menu->term_id, false )
                        . '>'. esc_html( $menu->name ) . '</option>';
                }
                ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('menu_3'); ?>"><?php _e('Select Menu:'); ?></label>
            <select id="<?php echo $this->get_field_id('menu_3'); ?>" name="<?php echo $this->get_field_name('menu_3'); ?>">
                <option value="0"><?php _e( '&mdash; Select &mdash;' ) ?></option>
                <?php
                foreach ( $menus as $menu ) {
                    echo '<option value="' . $menu->term_id . '"'
                        . selected( $menu_3, $menu->term_id, false )
                        . '>'. esc_html( $menu->name ) . '</option>';
                }
                ?>
            </select>
        </p>

        <?php
    }
}

add_action('widgets_init', create_function('', 'return register_widget("rm_multiple_menu_Widget");'));