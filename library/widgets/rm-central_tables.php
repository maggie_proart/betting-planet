<?php

/**
 * Show tables from rm-central
 *
 * @since 3.0.0
 */
class rm_central_tables_Widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'rm_central_tables_widget', // Base ID
            __('RaceMedia Tables', 'text_domain'), // Name
            array('description' => __('Add table from Rm-central', 'text_domain'),) // Args
        );
    }

    function widget($args, $instance)
    {
        extract($args);

        $title = apply_filters('widget_title', empty($instance['title']) ? __('Social') : $instance['title'], $instance, $this->id_base);
        /** This filter is documented in wp-includes/default-widgets.php */
        $instance['title'] = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

       $before_widget = $args['before_widget'];
        if (strpos($args['before_widget'], 'class') === false) {
            $before_widget = str_replace('>', 'class="' . $instance['extra-class'] . '"', $before_widget);
        } // there is 'class' attribute - append width value to it
        else {
            $before_widget = str_replace('class="', 'class="' . $instance['extra-class'] . ' ', $before_widget);
        }


        $helper = TableHelper::build_for_all();

        $scountry = "user";
        $simg = $helper->get_flag_img(true);
        if ($instance["country"] != "0") {
            $simg = $helper->get_flag_img(true, $instance['country']);
            $scountry = $instance["country"];
        }



        $img = "<img src='" . $simg . "' class='' style='float:right;max-height:20px;' title='".$helper->get_flag_title_text()."' alt='". $helper->get_flag_alt_text()."'/>";
        echo $before_widget;
        if (!empty($instance['title']))
            echo $args['before_title'] . $instance['title'] . $img . $args['after_title'];
        echo do_shortcode('[rm_casino_table def="' . $instance["data"] . '" view="' . $instance["view"] . '" country="' . $scountry . '"]');
        echo $args['after_widget'];
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        // Fields
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['view'] = strip_tags($new_instance['view']);
        $instance['data'] = strip_tags($new_instance['data']);
        $instance['country'] = strip_tags($new_instance['country']);
        $instance['extra-class'] = strip_tags($new_instance['extra-class']);
        return $instance;
    }

    function form($instance)
    {

        $title = '';
        $view = '';
        $data = '';
        $country = '';
        $extra_class = '';


        $helper = TableHelper::build_for_all();
        $dir = '/' . FileLib::file_to_web_path(__DIR__);
        $casino_ids = $helper->get_casinos_ids();
        $casino_ids = array_diff($casino_ids, array('template'));
        $default_table_file = basename($helper->get_default_table_path());

        $table_defs = array();
        $table_def_dir = FileLib::GET_CONF_LOCAL()->resolve_path('table-defs');
        rm_iterate_dir($table_def_dir, function ($file, &$table_defs) use ($default_table_file) {
            if (is_dir($file)) {
                return;
            }
            $file = basename($file);
            if ($default_table_file == $file) {
                $table_defs[] = 'default';
            } elseif (str_ends_with(strtolower($file), '.ini')) {
                $file = substr($file, 0, -4);
                $table_defs[] = basename($file);
            }
        }, null, true, $table_defs);
//swap default to start
        $ind = array_search('default', $table_defs);
        if ($ind) {
            $tmp = $table_defs[0];
            $table_defs[0] = $table_defs[$ind];
            $table_defs[$ind] = $tmp;
        }

        $table_views = array();
        $table_view_dir = FileLib::GET_CODE()->resolve_path('tables/views');
        rm_iterate_dir($table_view_dir, function ($file, &$table_views) {
            if (is_dir($file)) {
                return;
            }
            if (str_ends_with(strtolower($file), '.php')) {
                $file = substr($file, 0, -4);
                if (!str_ends_with($file, 'bjt-default')) {
                    $table_views[] = basename($file);
                }

            }
        }, null, true, $table_views);

        $table_view_local_dir = FileLib::GET_PUBLIC_DATA_LOCAL_ROOT()->resolve_path('tables/views');
        rm_iterate_dir($table_view_local_dir, function ($file, &$table_views) {
            if (is_dir($file)) {
                return;
            }
            if (str_ends_with(strtolower($file), '.php')) {
                $file = substr($file, 0, -4);
                $table_views[] = basename($file);
            }
        }, null, true, $table_views);
//swap default to start
        $ind = array_search('default', $table_views);
        if ($ind) {
            $tmp = $table_views[0];
            $table_views[0] = $table_views[$ind];
            $table_views[$ind] = $tmp;
        }
//cOUNTRIES
        $countries = CountriesArray::get('alpha2', 'name'); // returns alpha2->name array

        // Check values
        if ($instance) {
            $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
            $view = esc_attr($instance['view']);
            $data = esc_textarea($instance['data']);
            $country = esc_textarea($instance['country']);
            $extra_class = esc_textarea($instance['extra-class']);
        } ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>
        <p class="row">
            <label class="col-6"
                   for="<?php echo $this->get_field_id('data'); ?>"><?php _e('Data (what ini to use )', 'wp_widget_plugin'); ?></label>
            <select lass="col-6" id="<?php echo $this->get_field_id('data'); ?>"
                    name="<?php echo $this->get_field_name('data'); ?>">
                <option value="0"><?php _e('&mdash; Select &mdash;') ?></option>
                <?php
                foreach ($table_defs as $td) {
                    echo '<option value="' . $td . '"'
                        . selected($data, $td, false)
                        . '>' . esc_html($td) . '</option>';
                }
                ?>
            </select>
        </p>
        <p class="row"><label class="col-6"
                              for="<?php echo $this->get_field_id('view'); ?>"><?php _e('View:'); ?></label>
            <select class="col-6" id="<?php echo $this->get_field_id('view'); ?>"
                    name="<?php echo $this->get_field_name('view'); ?>">
                <option value="0"><?php _e('&mdash; Select &mdash;') ?></option>
                <?php
                foreach ($table_views as $tv) {
                    echo '<option value="' . $tv . '"'
                        . selected($view, $tv, false)
                        . '>' . esc_html($tv) . '</option>';
                }
                ?>
            </select></p>


        <p class="row">
            <label class="col-6"
                   for="<?php echo $this->get_field_id('country'); ?>"><?php _e('Country: AU/HK etc.. (leave blank if you want automatic GEO location )', 'wp_widget_plugin'); ?></label>
            <select class="col-6" id="<?php echo $this->get_field_id('country'); ?>"
                    name="<?php echo $this->get_field_name('country'); ?>">
                <option value="0"><?php _e('&mdash; Select &mdash;') ?></option>
                <?php
                foreach ($countries as $key => $value) {
                    echo '<option value="' . $key . '"'
                        . selected($country, $key, false)
                        . '>' . esc_html($value) . '</option>';
                }
                ?>
            </select>
        </p>

        <p>
            <label
                for="<?php echo $this->get_field_id('extra-class'); ?>"><?php _e('Custom css class:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('extra-class'); ?>"
                   name="<?php echo $this->get_field_name('extra-class'); ?>" type="text"
                   value="<?php echo $extra_class; ?>"/>
        </p>
    <?php }


}

add_action('widgets_init', create_function('', 'return register_widget("rm_central_tables_Widget");'));