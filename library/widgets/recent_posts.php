<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 4/08/2016
 * Time: 1:30 PM
 */

/**
 * Extend Recent Posts Widget
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 */
Class My_Recent_Posts_Widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'rm_recent_widget', // Base ID
            __('RaceMedia Recent posts ', 'text_domain'), // Name
            array('description' => __('Show recent posts', 'text_domain'),) // Args
        );
    }

    function widget($args, $instance)
    {

        extract($args);

        $title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts') : $instance['title'], $instance, $this->id_base);

        if (empty($instance['number']) || !$number = absint($instance['number']))
            $number = 10;

        $r = new WP_Query(apply_filters('widget_posts_args', array('posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true, 'category__not_in' => $instance['exc_category'] )));
        /** This filter is documented in wp-includes/default-widgets.php */
        $instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];

        if ( !empty($instance['title']) )
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
        if ($r->have_posts()) :

            ?>
            <ul class="widget_recent_entries">
                <?php while ($r->have_posts()) : $r->the_post(); ?>

                    <li class="item row">
                        <?php

                        $maxwords = 20;
                        $class = "small-12";
                        if (has_post_thumbnail()) :
                            $maxwords = 7;
                            $class = "small-8";

                            ?>
                            <div
                                class="media-section small-4  hide-for-medium-only"><?php echo get_the_post_thumbnail($r->ID, 'tiny_thumb', array()); ?></div>
                        <?php endif ?>
                        <div class="media-section  <?= $class ?>"><h4><a href="<?php the_permalink(); ?>"
                                                                         title="<?php the_title(); ?>"><?php echo wp_trim_words(wp_strip_all_tags(get_the_title()), $maxwords, '...'); ?></a>
                            </h4>
                            <?php if ($instance['show_date']):?>
                            <time class="date"><?php the_date('F j'); ?><?php the_time('g:i a'); ?>  </time>
                <?php endif ?>
                        </div>
                    </li>



                <?php endwhile; ?>
            </ul>

            <?php


            wp_reset_postdata();

        endif;
    }

    public function form($instance)
    {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $number = isset($instance['number']) ? absint($instance['number']) : 5;

        $exc_category = isset($instance['exc_category']) ? $instance['exc_category'] : [];
        $show_date = isset($instance['show_date']) ? (bool)$instance['show_date'] : false;
        $show_img = isset($instance['show_img']) ? (bool)$instance['show_img'] : false;

        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Exclude Categories:'); ?></label>

            <?php
            /* $categories  = get_categories();
         foreach ( $categories as $category ) { var_dump($category);}*/
            ?>
            <select class="widefat" id="<?php echo $this->get_field_id('exc_category'); ?>"
                    name="<?php echo $this->get_field_name('exc_category'); ?>[]" multiple>
                <?php $categories = get_categories();
                foreach ($categories as $category) { ?>
                    <option value="<?= $category->term_id ?>"  <?php echo in_array ($category->term_id , $exc_category) ? 'selected' : '' ?>> <?php echo esc_html($category->name); ?></option>
                    <?php
                }
                ?>
            </select>
        </p>

        <p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts to show:'); ?></label>
            <input class="tiny-text" id="<?php echo $this->get_field_id('number'); ?>"
                   name="<?php echo $this->get_field_name('number'); ?>" type="number" step="1" min="1"
                   value="<?php echo $number; ?>" size="3"/></p>

        <p><input class="checkbox" type="checkbox"<?php checked($show_date); ?>
                  id="<?php echo $this->get_field_id('show_date'); ?>"
                  name="<?php echo $this->get_field_name('show_date'); ?>"/>
            <label for="<?php echo $this->get_field_id('show_date'); ?>"><?php _e('Display post date?'); ?></label></p>
        <p><input class="checkbox" type="checkbox"<?php checked($show_date); ?>
                  id="<?php echo $this->get_field_id('show_img'); ?>"
                  name="<?php echo $this->get_field_name('show_img'); ?>"/>
            <label for="<?php echo $this->get_field_id('show_img'); ?>"><?php _e('Display post Image?'); ?></label></p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['number'] = (int)$new_instance['number'];
        $instance['exc_category'] = $new_instance['exc_category'];
        $instance['show_date'] = isset($new_instance['show_date']) ? (bool)$new_instance['show_date'] : false;
        $instance['show_img'] = isset($new_instance['show_img']) ? (bool)$new_instance['show_img'] : false;

        return $instance;
    }

}

function my_recent_widget_registration()
{
    unregister_widget('WP_Widget_Recent_Posts');
    register_widget('My_Recent_Posts_Widget');
}

add_action('widgets_init', 'my_recent_widget_registration');