<?php

// META BOXES
// Add the Meta Box
function add_casino_meta_box() {
   // $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    //$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
// check for a template type

    // Field Array

    $custom_meta_fields = array(
        array(
            'label'=> 'Casino ID',
            'desc'  => 'Casino ID in main RM',
            'id'    => 'r_casino_id',
            'type'  => 'text'
        ),
        array(
            'label'=> 'Introduction',
            'desc'  => 'Short Description.',
            'id'    => 'r_casino_introduction',
            'type'  => 'textarea'
        ),
    );

    add_meta_box(
        'casino_meta_box', // $id
        'Casino Details', // $title
        'show_custom_meta_box', // $callback
        'page', // $page
        'normal', // $context
        'high',  // $priority
        $custom_meta_fields);


}
add_action('add_meta_boxes', 'add_casino_meta_box');
function save_casino($postid){

    $custom_meta_fields = array(
        array(
            'label'=> 'Casino ID',
            'desc'  => 'Casino ID in main RM',
            'id'    => 'r_casino_id',
            'type'  => 'text'
        ),
        array(
            'label'=> 'Introduction',
            'desc'  => 'Short Description.',
            'id'    => 'r_casino_introduction',
            'type'  => 'textarea'
        ),
    );

    save_custom_meta($postid,$custom_meta_fields);
}
add_action('save_post', 'save_casino',10,1);


// The Callback
function show_custom_meta_box($post ,$setting) {

// Use nonce for verification
    echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

    // Begin the field table and loop
    echo '<table class="form-table">';
    foreach ($setting['args'] as $field) {

        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
        switch($field['type']) {
            // text
            case 'text':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
        <br /><span class="description">'.$field['desc'].'</span>';
                break;
            // textarea
            case 'textarea':
                echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea>
        <br /><span class="description">'.$field['desc'].'</span>';
                break;
            // checkbox
            case 'checkbox':
                echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>
        <label for="'.$field['id'].'">'.$field['desc'].'</label>';
                break;
            // select
            case 'select':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
                }
                echo '</select><br /><span class="description">'.$field['desc'].'</span>';
                break;

            case 'wp-editor':

                echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>
        <label for="'.$field['id'].'">'.$field['desc'].'</label>';
                break;

        } //end switch
        echo '</td></tr>';
    } // end foreach
    echo '</table>'; // end table
}





// Save the Data
function save_custom_meta($post_id,$custom_meta_fields) {

    // verify nonce
        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) )
            return $post_id;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    // loop through fields and save the data
    foreach ($custom_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = @$_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach

}




