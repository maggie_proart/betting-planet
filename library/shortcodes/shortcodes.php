<?php

function rm_row($params, $content = null) {

	extract(shortcode_atts(array(
		'title' => 'title',
		'row' => 1,
	), $params));
	
	return
		'<li class="accordion-navigation">
			<h2 id="panel'.$row.'d-heading" data-toggle="panel'.$row.'d panel'.$row.'d-heading" data-toggler="down">'
				.$title
			."</h2>"
			.'<div id="panel'.$row.'d" class="content" data-toggler="show">'
				.$content
			.'</div>'
		.'</li>';
}


add_shortcode('rm_row','rm_row');

function rmc_accordion($params, $content = null) {

	extract(shortcode_atts(array(
		'class' => 'class'
	), $params));
	

	return
		'<ul class="accordion ' . $class . '" data-accordion="">' . do_shortcode($content) . '</ul>';

}


add_shortcode('rm_accord','rmc_accordion');

function the_content_filter($content) {
    $block = join("|",array("rm_accord", "rm_row"));
    $rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
    $rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
return $rep;
}
add_filter("the_content", "the_content_filter");

/**
 * brmc_recent_news
 *
 * return list of recent post by parameters
 *
 * @categories (string coma sep) Categories to search in . empty will return all
 * @ex_categories (string coma sep) Categories to exclude
 * #tags (string) tags to search
 * @number_of_post (int) default 5
 * @post_status (string) default publish
 * @post_type (string) default post
 * @return (string)
 */
function brmc_recent_news($atts)
{
	$output = '';
	$attributes = shortcode_atts(
		array(
			'categories' => '',
			'ex_categories' => '',
			'tags' => '',
			'number_of_post' => 5,
			'post_status' => 'publish',
			'post_type' => 'post',
			'css_class' => '',
			'include_excerpt' => false,
			'excerpt_size' => 25,
			'thumbnail' => true,
			'show_date' => true,

		),
		$atts
	);


	$output .= brmc_get_recent_posts($attributes);

	return $output;
}

add_shortcode('recent_news', 'brmc_recent_news');


function rm_get_recent_news($atts){
    $output = '';
    $attributes = shortcode_atts(
        array(
            'categories' => '',
            'ex_categories' => '',
            'tags' => '',
            'exclude_tags' => null,
            'tag__and'=>null,
            'number_of_post' => 5,
            'post_status' => 'publish',
            'post_type' => 'post',
            'css_class' => '',
            'include_excerpt' => true,
            'excerpt_size' => 25,
            'thumbnail' => false,
            'show_date' => false,
            'title'=> 'Recent News',
            'url'=> '/',
            'ajax'=> false

        ),
        $atts
    );

    $output .= '<div class="recent-rm-news '.$attributes["css_class"].'">';
    $attributes["css_class"]='';
    $output .= '<h3>'.$attributes["title"].'</h3>';
    $response = rm_get_recent_posts($attributes);
    $output .= $response["output"];

    if ($attributes["ajax"]) {
        //only show button id there are more to get
        if ($response["count"]==$attributes["number_of_post"]) {
            $output .= '<button  data-box="" data-paged="1" data-q="' . base64_encode(json_encode($attributes)) . '" data-count"' . $attributes["number_of_post"] . '"  class="button hollow expend load_more_recent_news" title="More news & tips">More News</button>';
        }
    }
    else {
    $output .= '<a  href="'.$attributes["url"].'" class="button hollow expend" title="More news & tips">More News</a>';

    }

    $output .= '</div>';
    return $output;

}
add_shortcode('rm_recent_news','rm_get_recent_news');