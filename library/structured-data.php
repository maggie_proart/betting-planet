<?php

add_action('wp_head','rm_structured_data_head');

function rm_structured_data_head(){
	global $post;
	$breadcrumb = get_breadcrumb();


	$keywords = get_theme_mod('og-keywords', get_bloginfo('name') );
	$keywords_str = '';
	if(is_array($keywords)){
		foreach ($keywords as $keyword) {
			$keywords_str .= '"'.$keyword.'",';
		}
		$keywords_str = rtrim($keywords_str,",");
	}else{
		$keywords_str = '"'.$keywords.'"';
	}


	?>

	<script type="application/ld+json">
{
	"@context": "http://schema.org",
	<?php

		if(is_front_page()){
			?>

	"@type": "WebPage",
	"thumbnailUrl": "<?=  (file_exists(get_template_directory().'/assets/images/placeholder.jpg') ? get_template_directory_uri().'/assets/images/placeholder.jpg' : get_theme_mod('og-image',  esc_url( get_theme_mod( 'foundationpress_logo' ) ) ) ) ?>",
  "headline": "<?= get_bloginfo('description') ?>",
	"name" : "<?= get_bloginfo('name') ?>",
	"breadcrumb" : "<?= $breadcrumb ?>",
	"url": "<?= get_site_url() ?>",
	"description": "<?=   get_theme_mod('og-description', get_bloginfo('description'))  ?>",
	"keywords": [
		<?= $keywords_str ?>
	]

  <?php
		}else if(!is_front_page() && $post->post_type == 'post'){

	$description = '';
	$categories = get_the_category($post->ID);
	$category_list = '';
	$cats = get_the_category();
	for($i = 0; $i < count($cats) ; $i++) {
		$category_list .=  $cats[$i]->cat_name;
		if( isset($cats[$i+1]) ){
			$category_list .= ", ";
		}
	}


			$thumbURL = get_the_post_thumbnail_url($post->ID,"full")? get_the_post_thumbnail_url($post->ID,"full"): get_template_directory_uri().'/assets/images/placeholder.png';
			$thumbnail = getimagesize( $thumbURL );
			$thumbWidth = $thumbnail[0];
			$thumbHeight = $thumbnail[1];
			?>
	"@type": "NewsArticle",
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": "<?= get_permalink($post->ID) ?>",
      "breadcrumb" : "<?= $breadcrumb ?>"
    },
    "headline": "<?= $post->post_title ?>",
    "image": {
      "@type": "ImageObject",
      "url": "<?= $thumbURL ?>",
      "width": <?= ($thumbWidth?$thumbWidth:300) ?>,
      "height": <?= ($thumbHeight?$thumbHeight:300) ?>
    },
    "author": {
      "@type": "Person",
      "name": "<?= get_the_author_meta( 'display_name' , $post->post_author) ?>"
    },
    "publisher": {
      "@type": "Organization",
      "name": "<?= get_bloginfo('name') ?>",
      "logo": {
        "@type": "ImageObject",
        "url": "<?=   (file_exists(get_stylesheet_directory().'/assets/images/placeholder.jpg')? get_stylesheet_directory_uri().'/assets/images/placeholder.jpg' : get_theme_mod('og-image',  esc_url( get_theme_mod( 'foundationpress_logo' ) ) ) ) ?>",
        "width": 576,
        "height": 60
      }
    },
    "datePublished": "<?= $post->post_date ?>",
    "dateModified": "<?= $post->post_modified ?>",
    "url": "<?= get_permalink($post->ID) ?>",
    "thumbnailUrl": "<?= get_the_post_thumbnail_url($post->ID,"medium") ?>",
    "dateCreated": "<?= $post->post_date ?>",
    "articleSection": "<?= $category_list ?>",
    "creator": "<?= get_the_author_meta( 'display_name' , $post->post_author) ?>",
    "keywords": [<?=   ( !empty(wp_get_post_tags( $post->ID, array( 'fields' => 'names' ) ) ) ?
				implode('","',wp_get_post_tags( $post->ID, array( 'fields' => 'names' ) ) ) :
				$keywords_str );  ?> ]

    <?php } else if (is_page_template("page-templates/review.php") &&   get_post_custom_values('r_casino_id') !== false  ){
			$product_id = get_post_custom_values('r_casino_id');
			$product_id = $product_id[0];

			?>

      "@type":"Review",
  "author": {
      "@type": "Person",
      "name": "<?= get_the_author_meta( 'display_name' , $post->post_author) ?>"
    },
  "url": "<?= get_permalink($post->ID) ?>",
  "datePublished":"<?= $post->post_date ?>",
  "publisher": {
      "@type": "Organization",
      "name": "<?= get_bloginfo('name') ?>",
      "logo": {
        "@type": "ImageObject",
        "url": "<?=   (file_exists(get_stylesheet_directory().'/assets/images/placeholder.jpg')? get_stylesheet_directory_uri().'/assets/images/placeholder.jpg' : get_theme_mod('og-image',  esc_url( get_theme_mod( 'foundationpress_logo' ) ) ) ) ?>",
        "width": 576,
        "height": 60
      }
    },
  "description":"<?= addslashes( get_post_custom_values('r_casino_introduction')[0] )  ?>",
  "inLanguage":"en",
  "itemReviewed": {
    "@type":"Product",
    "name": "<?= TableHelper::build_for_casino($product_id)->get('display_name') ?>",
    "sameAs": "<?= TableHelper::build_for_casino($product_id)->get('site_url') ?>",
    "image": "<?= TableHelper::build_for_casino($product_id)->get_logo_img() ?>"
  },
  "reviewRating": {
     "@type":"Rating",
     "worstRating":1,
     "bestRating":10,
     "ratingValue":<?= TableHelper::build_for_casino($product_id)->get('editor_rating') ?>
  }


<?php  } else { ?>

    "@type": "WebPage",
  "thumbnailUrl": "<?=   (file_exists(get_template_directory().'/assets/images/placeholder.jpg') ? get_template_directory_uri().'/assets/images/placeholder.jpg' : get_theme_mod('og-image',  esc_url( get_theme_mod( 'foundationpress_logo' ) ) ) ) ?>",
  "headline": "<?= get_bloginfo('description') ?>",
  "name" : "<?= get_bloginfo('name') ?>",
  "breadcrumb" : "<?= $breadcrumb ?>",
  "url": "<?= ( get_permalink($post->ID) != '' ? get_permalink($post->ID) : get_site_url().$_SERVER[REQUEST_URI] ) ?>",
  "description": "<?=   get_theme_mod('og-description', get_bloginfo('description'))  ?>",
  "keywords": [
    <?= $keywords_str ?>
  ]

    <?php } ?>
}
</script>

	<?php
}


function get_breadcrumb() {
	$str = get_bloginfo('name');
	if (is_category() || is_single()) {
		$str .= " > ";
		$cats = get_the_category();
		for($i = 0; $i < count($cats) ; $i++) {
			$str .=  $cats[$i]->cat_name;
			if( isset($cats[$i+1]) ){
				$str .= ", ";
			}
		}

		if (is_single()) {
			$str .= "  >  ";
			$str .= html_entity_decode(get_the_title());
		}
	} elseif (is_page()) {
		$str .= " > ";
		$str .= html_entity_decode(get_the_title());
	} elseif (is_search()) {
		$str .= " > Search Results for... ";
		$str .= html_entity_decode( get_search_query() );
	}
	return $str;
}