<?php


if (!function_exists('get_rm_post_by_tag')) :
    function get_rm_post_by_tag($tag, $numberposts = 0, $alignment = 0)
    {
        global $posts_to_exclude;
        $posts_to_exclude = [];

        $post_type = array('post');
        if (is_front_page()) {
            array_push($post_type, "page ", " rm_games");
        }
        $args = array('numberposts' => $numberposts, 'post_status' => 'publish', 'tag' => $tag, 'post_type' => $post_type);
        $recent_posts = wp_get_recent_posts($args); ?>


        <div class="featured small-12 columns">
            <?php

            $x = 0;
            $a1 = "";
            $a2 = "";
            if ($alignment == 0) {
                $a1 = "large-4";
                $a2 = "large-8";
            }
            foreach ($recent_posts as $recent) {
                array_push($posts_to_exclude, $recent["ID"]);
                if ($x == 0) {

                    echo '<div class="main-section row collapse"><div class="img-wrapper small-12 ' . $a2 . ' columns">' . get_the_post_thumbnail($recent["ID"], "full") . '</div><div class="desc small-12 ' . $a1 . ' columns"><h2><a href="' . get_permalink($recent["ID"]) . '">' . wp_trim_words(wp_strip_all_tags($recent["post_title"]), 20, '...') . '</a></h2>   <div class="entry-meta hide-for-small-only">';

                    echo '</div> <div class="hide-for-small-only">' . wp_trim_words(wp_strip_all_tags(get_the_excerpt($recent["ID"])), 25, '...') . '</div> <div>';


                    echo '</div></div></div> ';
                    echo '<ul class="row collapse">';
                } else {
                    echo '<li class="columns"><div class="img-wrapper">' . get_the_post_thumbnail($recent["ID"], "medium") . '</div><div class="desc"><h3><a href="' . get_permalink($recent["ID"]) . '">' . wp_trim_words(wp_strip_all_tags($recent["post_title"]), 9, '...') . '</a></h3> <div>' ?>


                    <?php
                    //$post_author = get_userdata($recent['post_author']);
                    //echo '<span class="author">'. $post_author->display_name.'</span>';
                    ?>
                    <time
                        class="date"><?= get_the_date('F j', $recent["ID"]); ?>  <?= get_the_time('g:i a', $recent["ID"]); ?>  </time>
                    <?php echo '</div></div></li> ';

                }
                $x++;
            }
            ?>


            </ul>
        </div>
        <?php

    }
endif;

if (!function_exists('get_rm_featured_post')) :
    function get_rm_featured_post($atts, $justids = false)
    {
        $alignment = 0;

        $args = shortcode_atts(
            array(
                'categories_and' => '',
                'category' => '',
                'tag' => '',
                'category_name' => '',
                'posts_per_page' => 10,
                'post_status' => 'publish',
                'post_type' => array('post', 'page'),
                'post__not_in' => '',
                'category__in'=>null
            ),
            $atts
        );


        if (is_front_page()) {
            //  array_push($post_type, "page "," rm_games");
        }
        $recent_posts = wp_get_recent_posts($args);

        if ($justids) {
            return wp_list_pluck($recent_posts, 'ID');
        }


        ?>


        <ul class="featured">
            <?php
            $x = 0;
            foreach ($recent_posts as $recent) {
                //first post
                $class='';
                ?>
                <li class="small-12 columns">
                    <a href="<?= get_permalink($recent["ID"]) ?>" class="row">
                        <?php if ($x > 0) {
                            $class='small-4 medium-12';
                        }?>
                        <div class="img-wrapper <?=$class?>">
                            <?php
                            if (has_post_thumbnail($recent["ID"])) {
                                if($x==0){
                                    echo   get_the_post_thumbnail($recent["ID"]);
                                }
                                else {
                                    echo   get_the_post_thumbnail($recent["ID"], "medium");
                                }
                            }
                            else {
                                echo '<img alt="" title="'.get_the_title().'"  src="'.get_template_directory_uri() .'/assets/images/placeholder.jpg">';
                            }

                            ?>
                        </div>


                        <?php if ($x == 0) { ?>
                        <div class="desc">
                            <h2>
                                <?= wp_trim_words(wp_strip_all_tags($recent["post_title"]), 25, '...') ?>
                            </h2>
                            <div
                                class="hide-for-small-only"><?= wp_trim_words(wp_strip_all_tags(get_the_excerpt($recent["ID"])), 25, '...') ?></div>
                            <?php } else { ?>
                            <div class="desc small-8 medium-12">
                                <h3>
                                    <?= wp_trim_words(wp_strip_all_tags($recent["post_title"]), 12, '...') ?>
                                </h3>
                                <!--   <time
                                class="date"><?= get_the_date("F j", $recent["ID"]) ?> <?= get_the_time("g:i a", $recent["ID"]) ?> </time>-->
                                <?php } ?>

                            </div>
                    </a>
                </li>
                <?php
                $x++;
            }
            ?>
        </ul>


        <?php

    }
endif;


/**
 * brmc_get_recent_posts
 *
 * function will return a html with recent posts
 *
 * @args (Array) updated (array for the qp_query )
 * @return (string)
 */
function brmc_get_recent_posts($atts, $getids = false)
{

    $attributes = shortcode_atts(
        array(
            'categories_and' => null,
            'categories' => null,
            'ex_categories' => null,
            'tags' => null,
            'ex_tags' => null,
            'number_of_post' => 5,
            'post_status' => 'publish',
            'post_type' => 'post,page',
            'css_class' => null,
            'include_excerpt' => false,
            'excerpt_size' => 25,
            'thumbnail' => true,
            'show_date' => true,
            'show_author' => false,
            'hide_thumbnail' => array(),
            'post__not_in' => null,
            'tax_query' => null,
        ),
        $atts
    );


    $args = array(
        'numberposts' => $attributes["number_of_post"],
        'post_type' => explode(',', $attributes['post_type']),
        'post_status' => explode(',', $attributes["post_status"]),
        'tax_query' => @$attributes['tax_query']
    );
    // post not in
    if ($attributes["post__not_in"]) {
        $args["post__not_in"] = $attributes["post__not_in"];
    }
    //tags
    if ($attributes["tags"]) {
        // get the tags id
        $tags = explode(',', $attributes["tags"]);

        $tag_id_array = '';
        foreach ($tags as $tag) {
            $temp_tag = get_term_by("name", $tag, 'post_tag');
            if ($temp_tag) {
                $tag_id_array .= $temp_tag->term_id . ",";
            }
        }

        $args["tag__in"] = $tag_id_array;
    }
    // Categories
    if ($attributes["categories_and"]) {
        $cats = explode(',', $attributes["categories_and"]);
        $cat_id_array = [];
        foreach ($cats as $cat) {
            $temp_cat = get_term_by("name", $cat, 'category');
            if ($temp_cat) {
                array_push($cat_id_array, $temp_cat->term_id);
            }
        }
        $attributes["categories_and"] = $cat_id_array;
        //$args["category__and"]=$cat_id_array;
    }
    if ($attributes["categories"]) {
        // get the tags id
        $cats = explode(',', $attributes["categories"]);

        $cat_id_array = [];
        foreach ($cats as $cat) {
            $temp_cat = get_term_by("name", $cat, 'category');
            if ($temp_cat) {
                array_push($cat_id_array, $temp_cat->term_id);
            }
        }

        $attributes["categories"] = $cat_id_array;
    }
    if ($attributes["categories_and"] && $attributes["categories"]) {
        $args['tax_query'] = array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $attributes["categories_and"],
            ),
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $attributes["categories"],
                'include_children' => false,
            ),
        );
    } else {
        $args['category__in'] = $attributes["categories"];
        $args['category__and'] = $attributes["categories_and"];

    }
    $recent_posts = wp_get_recent_posts($args);

    if ($getids) {

        return wp_list_pluck($recent_posts, 'ID');
    }

    $output = '<ul class="news-ul row ' . $attributes["css_class"] . '">';
    // get recent news by country (tag) and by category
    $index = 0;
    foreach ($recent_posts as $recent) {
        $output .= '<li itemscope  class="small-12 columns"><a href="' . get_permalink($recent["ID"]) . '" class="row">';
        $output .= get_structured_data(['ID' => $recent["ID"], 'post_title' => $recent["post_title"], 'post_content' => $recent["post_excerpt"], 'post_excerpt' => $recent["post_excerpt"]]);
        $class = "medium-12";
        if ($attributes['thumbnail'] && has_post_thumbnail($recent["ID"])) {
            $class = "small-8";
            $imagesize = "medium";
            if ($attributes["css_class"] == "featured" && $index == 0) {
                $imagesize = "medium_large";
            }
            $output .= '<div class="thumbnail small-4 columns">' . get_the_post_thumbnail($recent["ID"], $imagesize, array()) . '</div>';

        }
        $output .= '<div class="' . $class . ' columns description">';
        $output .= ' <h4>' . $recent["post_title"] . '</h4>';
        if ($attributes['show_date'] == 'true') {
            $output .= '<time class="date" >' . get_the_date(' l jS F Y', $recent["ID"]) . ' </time>';

        }
        if ($attributes['show_author'] == 'true') {
            $output .= '<span class="gulp sassauthor">' . get_the_author() . '</span>';
        }
        if ($attributes['include_excerpt']) {
            $content = (empty ($recent["post_excerpt"])) ? $recent["post_content"] : $recent["post_excerpt"];
            $output .= '<p>' . wp_trim_words(wp_strip_all_tags(strip_shortcodes($content)), $attributes['excerpt_size'], '...') . '</p>';
        }
        $output .= '</div>';

        $output .= '</a></li> ';
        $index++;
    }
    $output .= '</ul>';
    wp_reset_query();

    return $output;
}


if (!function_exists('rmc_pagination')) :
    function rmc_pagination()
    {
        global $wp_query;

        $big = 999999999; // This needs to be an unlikely integer

        // For more options and info view the docs for paginate_links()
        // http://codex.wordpress.org/Function_Reference/paginate_links
        $paginate_links = paginate_links(array(
            'base' => str_replace($big, '%#%', html_entity_decode(get_pagenum_link($big))),
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages,
            'mid_size' => 5,
            'prev_next' => true,
            'prev_text' => __('&laquo;', 'rmc'),
            'next_text' => __('&raquo;', 'rmc'),
            'type' => 'list',
        ));

        $paginate_links = str_replace("<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links);
        $paginate_links = str_replace('<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links);
        $paginate_links = str_replace("<li><span class='page-numbers current'>", "<li class='current'><a href='#'>", $paginate_links);
        $paginate_links = str_replace('</span>', '</a>', $paginate_links);
        $paginate_links = str_replace("<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links);
        $paginate_links = preg_replace('/\s*page-numbers/', '', $paginate_links);

        // Display the pagination if more than one page is found.
        if ($paginate_links) {
            echo '<div class="pagination-centered">';
            echo $paginate_links;
            echo '</div><!--// end .pagination -->';
        }
    }
endif;

if (!function_exists('rmc_pagination_ajax')) :
    function rmc_pagination_ajax($posts_not_in = '', $number = 10,$query=null)
    {
        echo '<div class="pagination-centered">';
        $pni = $posts_not_in != "" ? implode("|", $posts_not_in) : "";

        echo '<div class="text-center"><button class="button-grey text-center" id="rm_page_load_more" data-query="'.$query.'" data-count="' . $number . '" data-box="' . $pni . '" data-paged="1">Load More</button></div>';
        echo '</div><!--// end .pagination -->';

    }
endif;


add_action('wp_ajax_nopriv_rm_ajax_pagination', 'rm_ajax_pagination');
add_action('wp_ajax_rm_ajax_pagination', 'rm_ajax_pagination');

function rm_ajax_pagination()
{
    $args='';
    $query = $_POST['query'];

    $message="No query";


    if ($query) {
        parse_str($query, $args);
        $message="we have a query";
    }

    else {
        $args = json_decode(stripslashes($_POST['query_vars']), true);}

    $args['paged'] = (int)$_POST['page'] + 1;

    $posts = new WP_Query($args);




    $GLOBALS['wp_query'] = $posts;

    if ($posts->have_posts()) {

        while ($posts->have_posts()) {
            $posts->the_post();
            get_template_part('content', 'archive');
        }
    } else {
        echo("no-more-posts");

    }
    wp_die();

}


function get_structured_data($recent)
{
    if (has_post_thumbnail($recent["ID"])) {
        $articleImage = get_the_post_thumbnail_url($recent["ID"], [768, round((768 / 1.959))]);
        $articleImageSize = [768, round((768 / 1.959))];
    } else {
        $articleImage = (file_exists(get_stylesheet_directory() . "/assets/images/placeholder.png") ? get_stylesheet_directory_uri() . "/assets/images/placeholder.png" : get_theme_mod("og-image", esc_url(get_theme_mod("foundationpress_logo"))));
        $articleImageSize = [179, 60];
    }
    return '<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "NewsArticle",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "' . get_permalink($recent["ID"]) . '"
  },
  "headline": "' . $recent["post_title"] . '",
  "image": {
    "@type": "ImageObject",
    "url": "' . $articleImage . '",
    "height": ' . $articleImageSize[1] . ',
    "width": ' . $articleImageSize[0] . '
  },
  "datePublished": "' . get_the_date('c', $recent["ID"]) . '",
  "dateModified": "' . get_the_modified_date('c', $recent["ID"]) . '",
  "author": {
    "@type": "Person",
    "name": "' . get_user_meta(get_post_field('post_author', $recent["ID"]), 'nickname', true) . '"
  },
   "publisher": {
      "@type": "Organization",
      "name": "' . get_bloginfo("name") . '",
      "logo": {
        "@type": "ImageObject",
        "url": "' . (file_exists(get_stylesheet_directory() . "/assets/images/placeholder.png") ? get_stylesheet_directory_uri() . "/assets/images/placeholder.png" : get_theme_mod("og-image", esc_url(get_theme_mod("foundationpress_logo")))) . '",
        "width": 179,
        "height": 60
      }
    },
  "description": "' . wp_trim_words(wp_strip_all_tags(strip_shortcodes(((empty ($recent["post_excerpt"])) ? $recent["post_content"] : $recent["post_excerpt"]))), 25, '...') . '"
}
</script>';
}

add_filter('get_custom_logo', 'remove_itemprop');


function remove_itemprop($html)
{
    $html = str_replace('itemprop="logo"', '', $html);
    return $html;
}

function brmc_social()
{
    if (is_single() || is_page()) {
        //$custom_content = $content;
        echo '<div class="dc-social row"><a  target="_blank"  href="http://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_permalink()) . '&title=' . urlencode(get_the_title()) . '" class="small-6 columns facebook-button"><i class="fa fa-facebook" aria-hidden="true"></i> Share On Facebook</a><a  href="http://twitter.com/intent/tweet?status=' . urlencode(get_the_title()) . '+' . urlencode(get_permalink()) . '" class="small-6 columns twitter-button" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> Share On Twitter</a></div>';


    }
}

add_filter('rm_page_before_comments', 'brmc_social', 99);

function bp_get_social(){
    $html='<ul class="social-list">';
    $html.= get_theme_mod('bp-facebook')?'<li><a href="' . get_theme_mod('bp-facebook') . '"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>':'';
    $html.= get_theme_mod('bp-twitter')?'<li><a href="' . get_theme_mod('bp-twitter') . '"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>':'';
    $html.= get_theme_mod('bp-google')?'<li><a href="' . get_theme_mod('bp-google') . '"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>':'';
    $html.='</ul>';
    echo $html;

}


/*------------------------------------------------------------------------------*/
/* Add title tag to image */
function rm_add_img_title( $attr, $attachment = null ) {

    $img_title = str_replace("_"," ",trim( strip_tags( $attachment->post_title ) )); ;

    $attr['title'] = preg_match_all( "/[0-9]/", $img_title ) < 8 ?$img_title: the_title_attribute( 'echo=0' );

    return $attr;
}
add_filter( 'wp_get_attachment_image_attributes','rm_add_img_title', 10, 2 );




/*
Not sure how this differes from the one up the top of this file

 * brmc_get_recent_posts
 *
 * function will return a html with recent posts
 *
 * @args (Array) updated (array for the qp_query )
 * @return (string)
 */
 function rm_get_recent_posts($atts, $getids = false)
 {
 
     $attributes = shortcode_atts(
         array(
             'categories_and' => null,
             'categories' => null,
             'ex_categories' => null,
             'tags' => null,
             'exclude_tags' => null,
             'tag__and'=>null,
             'number_of_post' => 5,
             'post_status' => 'publish',
             'post_type' => 'post,page',
             'css_class' => null,
             'include_excerpt' => false,
             'excerpt_size' => 25,
             'thumbnail' => true,
             'show_date' => true,
             'show_author' => false,
             'hide_thumbnail' => array(),
             'post__not_in' => null,
             'tax_query' => null,
             'paged'=> 1
         ),
         $atts
     );
 
 
 
     $args = array(
         'posts_per_page' => $attributes["number_of_post"],
         'post_type' => explode(',', $attributes['post_type']),
         'post_status' => explode(',', $attributes["post_status"]),
         'tax_query' => @$attributes['tax_query'],
         'paged'=> $attributes['paged']
     );
     // post not in
     if ($attributes["post__not_in"]) {
         $args["post__not_in"] = $attributes["post__not_in"];
     }
     //tags
     if ($attributes["tags"]) {
         // get the tags id
         $tags = explode(',', $attributes["tags"]);
 
         $tag_id_array = '';
         foreach ($tags as $tag) {
             $temp_tag = get_term_by("name", $tag, 'post_tag');
             if ($temp_tag) {
                 $tag_id_array .= $temp_tag->term_id . ",";
             }
         }
 
         $args["tag__in"] = $tag_id_array;
     }
     if ($attributes["tag__and"]) {
 
         $tags = explode(',', $attributes["tag__and"]);
 
         $tag_id_array = [];
         foreach ($tags as $tag) {
             $temp_tag = get_term_by("name", $tag, 'post_tag');
             if ($temp_tag) {
                 array_push($tag_id_array, $temp_tag->term_id);
             }
         }
         $args["tag__and"] = $tag_id_array;
         //$args["category__and"]=$cat_id_array;
     }
     if ($attributes["exclude_tags"]) {
         // get the tags id
         $tags = explode(',', $attributes["exclude_tags"]);
 
         $tag_id_array = '';
         foreach ($tags as $tag) {
             $temp_tag = get_term_by("name", $tag, 'post_tag');
             if ($temp_tag) {
                 $tag_id_array .= $temp_tag->term_id . ",";
             }
         }
 
         $args["tag__not_in"] = $tag_id_array;
     }
     // Categories
     if ($attributes["categories_and"]) {
         $cats = explode(',', $attributes["categories_and"]);
         $cat_id_array = [];
         foreach ($cats as $cat) {
             $temp_cat = get_term_by("name", $cat, 'category');
             if ($temp_cat) {
                 array_push($cat_id_array, $temp_cat->term_id);
             }
         }
         $attributes["categories_and"] = $cat_id_array;
         //$args["category__and"]=$cat_id_array;
     }
     if ($attributes["categories"]) {
         // get the tags id
         $cats = explode(',', $attributes["categories"]);
 
         $cat_id_array = [];
         foreach ($cats as $cat) {
             $temp_cat = get_term_by("name", $cat, 'category');
             if ($temp_cat) {
                 array_push($cat_id_array, $temp_cat->term_id);
             }
         }
 
         $args["categories"] = $cat_id_array;
     }
     if ($attributes["categories_and"] && $attributes["categories"]) {
         $args['tax_query'] = array(
             'relation' => 'AND',
             array(
                 'taxonomy' => 'category',
                 'field' => 'term_id',
                 'terms' => $attributes["categories_and"],
             ),
             array(
                 'taxonomy' => 'category',
                 'field' => 'term_id',
                 'terms' => $attributes["categories"],
                 'include_children' => false,
             ),
         );
     } else {
         $args['category__in'] = $attributes["categories"];
         $args['category__and'] = $attributes["categories_and"];
 
     }
     $recent_posts = new WP_Query( $args );
 
 
     if ($getids) {
 
         return wp_list_pluck($recent_posts, 'ID');
     }
 
 
     // get recent news by country (tag) and by category
     $index = 0;
     $recent_posts = new WP_Query( $args );
 
 // The Loop
     if ( $recent_posts->have_posts() ) {
         $output = '<ul class="news-ul row ' . $attributes["css_class"] . '">';
         while ( $recent_posts->have_posts() ) {
             $recent_posts->the_post();
             $output .= '<li itemscope  class="small-12 columns"><a href="' . get_permalink() . '" class="row">';
             $output .= get_structured_data(['ID' => get_the_ID(), 'post_title' => get_the_title(), 'post_content' => get_the_content(), 'post_excerpt' => get_the_excerpt()]);
             $class = "medium-12";
             if ($attributes['thumbnail'] && has_post_thumbnail()) {
                 $class = "small-8";
                 $imagesize = "medium";
                 if ($attributes["css_class"] == "featured" && $index == 0) {
                     $imagesize = "medium_large";
                 }
                 $output .= '<div class="thumbnail small-4 columns">' . get_the_post_thumbnail( $imagesize, array()) . '</div>';
 
             }
             $output .= '<div class="' . $class . ' columns description">';
             $output .= ' <h4>' . get_the_title(). '</h4>';
             if ($attributes['show_date'] == 'true') {
                 $output .= '<time class="date" >' . get_the_date(' l jS F Y') . ' </time>';
 
             }
             if ($attributes['show_author'] == 'true') {
                 $output .= '<span class="gulp sassauthor">' . get_the_author() . '</span>';
             }
             if ($attributes['include_excerpt']) {
                 $content = (empty (get_the_excerpt())) ? get_the_content() : get_the_excerpt();
                 $output .= '<p>' . wp_trim_words(wp_strip_all_tags(strip_shortcodes($content)), $attributes['excerpt_size'], '...') . '</p>';
             }
             $output .= '</div>';
 
             $output .= '</a></li> ';
         }
         $output .= '</ul>';
         /* Restore original Post Data */
         wp_reset_postdata();
     } else {
         $output = '';
     }
 
     $count = $recent_posts->post_count;
     $response =[];
     $response["count"]=$count;
     $response["output"]=$output;
     wp_reset_query();
 
     return $response;
 }