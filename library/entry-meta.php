<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 5/09/2016
 * Time: 6:06 PM
 */

if ( ! function_exists( 'rm_entry_meta' ) ) :
    function rm_entry_meta() {
        echo '<div class="entry-meta row"><i class="small-6  columns"><time class="" datetime="' . get_the_time( 'c' ) . '">' . sprintf( __( ' %s  %s.', 'rmb' ), get_the_date(), get_the_time() ) . '</time></i>';
        echo '<span class="byline author small-6  columns text-right">' . __( 'by', 'rmb' ) . ' <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" rel="author" class="fn">' . get_the_author() . '</a></div>';
    }
endif;
