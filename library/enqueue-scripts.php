<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 5/09/2016
 * Time: 12:07 PM
 */

// add scripts
if ( ! function_exists( 'rm_scripts' ) ) :
    function rm_scripts() {
    	global $wp_query;

        // Deregister the jquery version bundled with WordPress.
        wp_deregister_script( 'jquery' );
        //wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/css/big/foundation.css', array(), '2.6.1', 'all' );
        // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
        wp_enqueue_script( 'jquery_custom', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0' );
       // wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/js/foundation.min.js', array('jquery_custom'), '2.6.1', true );
        wp_register_script('rm',get_template_directory_uri() . '/assets/js/app.js');
		wp_localize_script( 'rm', 'rmc_ajax', array(
		    'ajaxurl' => admin_url( 'admin-ajax.php' ),
		    'query_vars' => json_encode( $wp_query->query )
		));

        wp_enqueue_script( 'owl', get_template_directory_uri() . '/assets/js/owl-carousel.min.js', array('jquery_custom'), '2.6.1', true );
        wp_enqueue_script( 'rm', get_template_directory_uri() . '/assets/js/app.js', array('jquery_custom'), '2.6.1', true );



    }
endif;

add_action( 'wp_enqueue_scripts', 'rm_scripts' );

//async script (js)

function remove_unwanted_css(){
    wp_dequeue_style('apester_font_css');
    wp_dequeue_style('apester_tiny_mce_vendor_css');
    wp_dequeue_style('apester_tiny_mce_css');
}
add_action('init','remove_unwanted_css', 100);



function make_script_async( $tag, $handle, $src )
{
    $async = ["jquery","dc-tinymce.js","lfapps.js","load-css-async","qmerce_js_sdk"];
    $atts = "";
    foreach($async as $file){
        if($file==$handle){
            $atts=" defer";
        }
    }


    //async defer
    return '<script src="' . $src . '"  type="text/javascript" '.$atts.'></script>' . "\n";


}
//add_filter( 'script_loader_tag', 'make_script_async', 10, 3 );