<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 31/08/2016
 * Time: 3:48 PM
 */

if ( ! function_exists( 'rmc_sidebar_widgets' ) ) :
    function rmc_sidebar_widgets() {
        register_sidebar(array(
            'id' => 'sidebar-widgets',
            'name' => __( 'Sidebar widgets', 'rmc' ),
            'description' => __( 'Drag widgets to this sidebar container.', 'rmc' ),
            'before_widget' => '<article id="%1$s" class="widget %2$s">',
            'after_widget' => '</article>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'id' => 'sidebar-left-widgets',
            'name' => __( 'Left Sidebar widgets', 'rmb' ),
            'description' => __( 'Drag widgets to this sidebar container.', 'rmc' ),
            'before_widget' => '<article id="%1$s" class="widget %2$s">',
            'after_widget' => '</article>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'id' => 'footer-top-widgets',
            'name' => __( 'Footer Top widgets', 'rmc' ),
            'description' => __( 'Drag widgets to this footer container', 'rmc' ),
            'before_widget' => '<article id="%1$s" class="columns widget %2$s">',
            'after_widget' => '</article>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));
        register_sidebar(array(
            'id' => 'footer-bottom-widgets',
            'name' => __( 'Footer Bottom widgets', 'rmc' ),
            'description' => __( 'Drag widgets to this footer container', 'rmc' ),
            'before_widget' => '<article id="%1$s" class="columns widget %2$s">',
            'after_widget' => '</article>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));
    }

    add_action( 'widgets_init', 'rmc_sidebar_widgets' );
endif;

/***
 * Ad css class  to all widget in order
 */
function brmc_add_css_widget_form($t,$return,$instance){
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'cssClass' => '') );

    if ( !isset($instance['cssClass']) )
        $instance['cssClass'] = null;
    ?>
    <p>
        <label for="<?php echo $t->get_field_id('cssClass'); ?>">cssClass:</label>

        <input type="text" name="<?php echo $t->get_field_name('cssClass'); ?>" id="<?php echo $t->get_field_id('cssClass'); ?>" value="<?php echo $instance['cssClass'];?>" />
    </p>
    <?php
    $retrun = null;
    return array($t,$return,$instance);
}

function brmc_widget_form_update($instance, $new_instance, $old_instance){

    $instance['cssClass'] = strip_tags($new_instance['cssClass']);
    return $instance;
}
function brmc_dynamic_sidebar_params($params){
    global $wp_registered_widgets;
    $widget_id = $params[0]['widget_id'];
    $widget_obj = $wp_registered_widgets[$widget_id];
    $widget_opt = get_option($widget_obj['callback'][0]->option_name);
    $widget_num = $widget_obj['params'][0]['number'];
    if (isset($widget_opt[$widget_num]['cssClass'])){
        $cssClass = $widget_opt[$widget_num]['cssClass'];
        $params[0]['before_widget'] = preg_replace('/class="/', 'class="'.$cssClass.' half ',  $params[0]['before_widget'], 1);
    }
    return $params;
}

//Add input fields(priority 5, 3 parameters)
add_action('in_widget_form', 'brmc_add_css_widget_form',5,3);
//Callback function for options update (priorität 5, 3 parameters)
add_filter('widget_update_callback', 'brmc_widget_form_update',5,3);
//add class names (default priority, one parameter)
add_filter('dynamic_sidebar_params', 'brmc_dynamic_sidebar_params');