<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 */

get_header(); ?>
<?php
global $paged;

?>


    <div class="row main news" role="main">
        <div class="main-content small-12 columns medium-9">
            <header>
                <?php if ( is_home() && ! is_front_page() ) : ?>
                    
                        <h1 class="entry-title"><?php single_post_title(); ?></h1>
                
                <?php else : ?>
                
                    <h2 class="entry-title"><?php the_title(); ?></h2>
                
                <?php endif; ?>               
            </header>
            <?php
            $idObj = get_category_by_slug('news');
            $id = $idObj->term_id;
            $notin = get_category_by_slug('racing');
            $notinid = $notin->term_id;

            $atts = array(
                'category__in' => $id,
                'post_status' => 'publish',
                'paged' => 1,
                'category__not_in' => $notinid,
                'post_type' => 'post',
                'posts_per_page' => 3

            );
            $posts_to_exclude = get_rm_featured_post($atts, true);
            ?>
            <!--featured-->
            <div class="row">
                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $atts['post__not_in'] = $posts_to_exclude;

                ?>

                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $posts_to_exclude= array_merge($posts_to_exclude,get_rm_featured_post($atts,true));
                $atts['post__not_in'] = $posts_to_exclude;

                ?>
                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $posts_to_exclude= array_merge($posts_to_exclude,get_rm_featured_post($atts,true));
                $atts['post__not_in'] = $posts_to_exclude;

                ?>
                <div class="medium-6 small-12 columns"><?php get_rm_featured_post($atts); ?></div>
                <?php $posts_to_exclude= array_merge($posts_to_exclude,get_rm_featured_post($atts,true));
                ?>
            </div>
            <?php

            $args = array(
                'category' => $id,
                'post__not_in' => $posts_to_exclude,
                'post_status' => 'publish',
                'paged' => 1,


            );

            $the_query = new WP_Query($args);

            ?>

            <?php while ($the_query->have_posts()):
                $the_query->the_post();
                get_template_part('content', 'archive');

            endwhile; ?>
            <?php if (function_exists('rmc_pagination_ajax')) {
                ?>
                <div class="small-12 ">
                    <?php

                    rmc_pagination_ajax(null, 25, http_build_query($args)); ?>
                </div>
                <?php
            } else if (is_paged()) { ?>
                <nav id="post-nav">
                    <div
                        class="post-previous"><?php next_posts_link(__('&larr; Older posts', 'foundationpress')); ?></div>
                    <div
                        class="post-next"><?php previous_posts_link(__('Newer posts &rarr;', 'foundationpress')); ?></div>
                </nav>
            <?php } ?>
        </div>

        <aside class="columns small-12 medium-3 ">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>