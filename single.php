<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 */

get_header(); ?>


    <div class="row main" role="main">
        <article class="main-content small-12 columns medium-9">
            <?php while (have_posts()) : the_post(); ?>
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php rm_entry_meta(); ?>
                </header>

                <?php the_content(); ?>
                <footer>

                    <?php wp_link_pages(); ?>

                    <div class="text-center row next-prev-links">
                        <div class="columns small-6">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            &nbsp; <?php previous_post_link('%link', 'Previous in category', TRUE); ?>
                        </div>
                        <div class="columns small-6">
                            <?php next_post_link('%link', 'Next post in category', TRUE); ?> &nbsp; <i
                                class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </footer>
                <?php do_action('rm_page_before_comments'); ?>
                <?php comments_template(); ?>
                <?php do_action('rm_page_after_comments'); ?>
            <?php endwhile; ?>

            <div class="more-news">
                <h3>More News <span class="line"></span></h3>
                <ul class="row">
                    <?php
                    //for use in the loop, list 5 post titles related to first tag on current post
                    $tags = wp_get_post_tags($post->ID);
                    if ($tags) {

                        $first_tag = $tags[0]->term_id;
                        $args=array(
                            'tag__in' => array($first_tag),
                            'post__not_in' => array($post->ID),
                            'posts_per_page'=>8,
                            'caller_get_posts'=>1
                        );
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) : $my_query->the_post(); ?>
                                <li class="small-6 medium-4 large-3 columns">
                                    <a href="<?php the_permalink();?>">
                                    <div class="img-wrapper small-12">
                                        <?php
                                        if (has_post_thumbnail(get_the_ID())) {
                                            echo   get_the_post_thumbnail(get_the_ID(), "medium");
                                        }
                                        else {
                                            echo '<img alt="" title="'.get_the_title().'"  src="'.get_template_directory_uri() .'/assets/images/placeholder.jpg">';
                                        }

                                        ?>
                                    </div>
                                    <div class="desc small12">
                                        <h3>
                                            <?= wp_trim_words(wp_strip_all_tags(get_the_title()), 12, '...') ?>
                                        </h3>
                                        </div>
                                    </a>
                                </li>

                                <?php
                            endwhile;
                        }
                        wp_reset_query();
                    }
                    ?>

                </ul>

                <a href="/news" class="button-grey row columns">All Recent News</a>
            </div>
        </article>
        <aside class="columns small-12 medium-3">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>