/**
 * Created by Maggie on 5/09/2016.
 */

$(document).foundation();

jQuery(document).ready(

    function(){
        jQuery('.event-date-title').each(function( index ) {
            var d = get_time_diff(jQuery( this ).attr('data-time'));
        });
        adjust_menu();
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:4,
            responsiveClass:true,
            nav:true,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive:{
                0:{
                    items:2,

                },
                600:{
                    items:3,

                },
                1000:{
                    items:5,

                    loop:false
                }
            }
        });
        var windowWidth = $(window).width();

//on resize close menu  (uncheck checkbox)
        $( window ).resize(function() {
            console.log('resizing');
            if ($(window).width() != windowWidth) {

                // Update the window width for next time
                windowWidth = $(window).width();
                if ($("#mobile-icon").hasClass("light")){
                $("#mobile-icon").click();
                }
                $(".mobile-menu-button").attr('checked', false);
                $( "#menu-mobile li input:checkbox").attr('checked', false);
            }

        });
    }



);
function adjust_menu (){
    iseven = (($("#menu-main > li").length)%2 == 0)
var  middle = Math.ceil($("#menu-main > li").length/2);

    $( "#menu-main > li:nth-child("+middle+ ")").addClass("highlight")
    $( "#menu-main > li:nth-child("+(middle+1)+ ")").addClass("highlight")
    $( "#menu-main > li:nth-child("+(middle-1)+ ")").addClass("highlight")
    if(iseven){
        $( "#menu-main > li:nth-child("+(middle+2)+ ")").addClass("highlight")
    }

}
function get_time_diff( datetime )
{
    var datetime = typeof datetime !== 'undefined' ? datetime : "2014-21-01 01:02:03.123456";
    console.log("the event date is "+datetime);
    var datetime = new Date(datetime).getTime();
    console.log("the event date is "+datetime);
    var now = new Date().getTime();
    console.log("the now date is "+datetime);


    if( isNaN(datetime) )
    {

        return "";
    }


    if (datetime < now) {
        var milisec_diff = now - datetime;
    }else{
        var milisec_diff = datetime - now;
    }




    console.log( milisec_diff );
    var diff='';
    console.log (days + " Days "+ date_diff.getHours() + " Hours " + date_diff.getMinutes() + " Min" + date_diff.getSeconds() + " Seconds");


    return "pp"; //days + " Days "+ date_diff.getHours() + " Hours " + date_diff.getMinutes() + " Min" + date_diff.getSeconds() + " Seconds";
}

jQuery(function($){

        $('#rm_page_load_more').on('click',function(){
            var $this = $(this);

                $.ajax({
                    url : rmc_ajax.ajaxurl,
                    type : 'post',
                    data : {
                        action : 'rm_ajax_pagination',
                        query_vars: rmc_ajax.query_vars,
                        page: $this.data('paged'),
                        query: $this.data('query')
                    },
                    beforeSend: function(){
                        $this.css({'opacity':'0.65','cursor':'not-allowed'});
                    },
                    success: function( response ){
                        // do stuff

                        $('.archive-item:last').after(response);
                        $this.css({'opacity':'1','cursor':'pointer'});
                        $this.data('paged', parseInt($this.data('paged')) + 1);
                    }
                })
        });

});