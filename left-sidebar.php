<?php
/*
Template Name:Left sidebar
*/

get_header(); ?>


    <div class="left-sidebar page row main" role="main">
        <aside id="left-sidebar" class=" columns small-12 medium-3 small-order-2  medium-order-1">
            <?php dynamic_sidebar('sidebar-left-widgets'); ?>
        </aside>
        <article class="main-content small-12 columns medium-9  small-order-1 medium-order-2 ">
            <?php while (have_posts()) : the_post(); ?>
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <?php the_content(); ?>


                <?php do_action('rm_page_before_comments'); ?>
                <?php comments_template(); ?>
                <?php do_action('rm_page_after_comments'); ?>
            <?php endwhile; ?>
        </article>

    </div>

<?php get_footer(); ?>