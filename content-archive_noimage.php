<?php
/**
 * The template part for displaying article content
 *
 * @package WordPress
 * @subpackage RMC
 * @since RMC 1.0
 */
?>

<div class="archive-item">
    <a href=<?php get_the_permalink()?>>
    <?php echo get_structured_data(['ID' => get_the_ID(), 'post_title'=>get_the_title(), 'post_content'=>get_the_excerpt(), 'post_excerpt'=>get_the_excerpt() ]); ?>

    <div class="desc small-12">
        <h3><?= get_the_title() ?></h3>
        <div>
            <time
                class="date"><?= get_the_date("j F", get_the_ID()) ?> <?= get_the_time("g:i a", get_the_ID()) ?> </time>
           <div class="description"> <?php echo wp_trim_words( wp_strip_all_tags(get_the_excerpt()), 25, '...' ) ?></div>
        </div>

    </div>
        </a>
</div>

