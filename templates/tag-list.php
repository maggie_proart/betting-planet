<?php
/**
*Template Name: Tag-list
*/
get_header(); ?>


    <div class="row main" role="main">
        <div class="main-content small-12 columns medium-9" >
            <?php while ( have_posts() ) : the_post(); ?>
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <?php
                $tags = get_tags();
                $html = '<ul class="tags-list">';
                foreach ($tags as $tag) {

                    $metadesc = '';
                    $wds_title = '';
                    if (function_exists('wds_get_term_meta')) {
                        $metadesc = wds_get_term_meta($tag, 'post_tag', 'wds_desc');
                        $wds_title = wds_get_term_meta($tag, 'post_tag', 'wds_title');
                    }

                    $tag_link = get_tag_link($tag->term_id);
                    $html .= "<li><a href='{$tag_link}' class='{$tag->slug}'>";
                    $html .= "{$tag->name}  </a><p>" . $metadesc . "</p></li>";



                }
                $html .= '</ul>';
                echo $html;


                ?>

                <footer>

                    <?php wp_link_pages(); ?>


                </footer>

            <?php endwhile;?>
        </div>
        <aside class="columns small-12 medium-3">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>