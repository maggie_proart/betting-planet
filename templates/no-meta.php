<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 * Template Name: No meta
 */

get_header(); ?>


    <div class="row main" role="main">
        <article class="main-content small-12 columns medium-9" >
            <?php while ( have_posts() ) : the_post(); ?>
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>

                </header>

                <?php the_content();?>

                <footer>

                    <?php wp_link_pages(); ?>

                    <div class="text-center row next-prev-links">
                        <div class="columns small-6">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> &nbsp; <?php previous_post_link('%link', 'Previous in category', TRUE); ?>
                        </div>
                        <div class="columns small-6">
                            <?php next_post_link( '%link', 'Next post in category', TRUE ); ?> &nbsp; <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </footer>
                <?php do_action( 'rm_page_before_comments' ); ?>
                <?php comments_template(); ?>
                <?php do_action( 'rm_page_after_comments' ); ?>
            <?php endwhile;?>
        </article>
        <aside class="columns small-12 medium-3">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>