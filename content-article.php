<?php
/**
 * The template part for displaying article content
 *
 * @package WordPress
 * @subpackage RMC
 * @since RMC 1.0
 */
?>

<article class="row collapse">

    <?php echo get_structured_data(['ID' => get_the_ID(), 'post_title'=>get_the_title(), 'post_content'=>get_the_excerpt(), 'post_excerpt'=>get_the_excerpt() ]); ?>
            <?php $class="small-12" ?>
            <?php if ( has_post_thumbnail() ) : ?>
                <a  class="small-3 columns" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php echo get_the_post_thumbnail(null,'medium'); ?>

                </a>
                <?php $class="small-9" ?>
            <?php endif; ?>

            <div class="<?=$class?> columns">
                <h3><a href="<?php echo get_the_permalink()?>"><?php echo get_the_title()?></a></h3>
                <div class="description hide-for-small-only"> <?php echo wp_trim_words( wp_strip_all_tags(get_the_excerpt()), 25, '...' ) ?></div>
                <footer class="hide-for-small-only">
                    <?php rm_entry_meta(); ?>
                </footer>
            </div>
</article>

