<?php
/**
 * Created by PhpStorm.
 * User: Maggie
 * Date: 30/08/2016
 * Time: 3:19 PM
 */

get_header();
 global $posts_to_exclude;

?>


    <div class="row main" role="main">
        <div class="main-content small-12 columns medium-9" >
             <div class="row">
                 <aside id="left-sidebar" class="columns small-12 hide-for-medium-only large-3 small-order-3 medium-order-1">
                     <?php dynamic_sidebar('sidebar-left-widgets'); ?>
                 </aside>
                 <div class=" small-12  medium-12 large-9 small-order-1 medium-order-2 main-front" >
                   <?php  $atts =array (
                     'post_status' => 'publish',
                     'tag'=>'featured',
                     'posts_per_page' => 4,
                       'post_type'=>array('rm_games','post','page')
                     );

                     $posts_to_exclude=get_rm_featured_post($atts,true);?>

                     <?php  get_rm_featured_post($atts);?>
                     <div class="latest-news small-12 columns">
                     <ul >
                         <?php
                         $args = array( 'numberposts' => '5','post_status' => 'publish','category__not_in' => array( 4 ),'post__not_in'=>$posts_to_exclude );
                         $recent_posts = wp_get_recent_posts( $args );
                         $index=0;
                         foreach( $recent_posts as $recent ){
                             $class="small-4";
                             if ($index==0)  $class="small-12 first";
                             echo '<li class="row collapse"><div class="img-wrapper '.$class.' medium-2 columns">';
                             if ( has_post_thumbnail($recent["ID"]) ) {
                                 echo get_the_post_thumbnail( $recent["ID"], 'med-rmc' ,array('class' => ''));

                             }
                             $class="small-8";
                             if ($index==0)  $class="small-12 first";
                             echo'</div><a class="'.$class.' medium-7 columns link-title" href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a>';
                             echo '  <time class="date small-3 columns text-right hide-for-small-only">'.get_the_date('F j',$recent["ID"]) .' '.get_the_time('g:i a',$recent["ID"]).'</time>';
                             echo'</li> ';
                             $index++;
                         }
                         wp_reset_query();
                         ?>
                     </ul>
                     <a href="/news/" class="button-blue row">More news</a>
                     </div>
                     </div>
                 <div class=" small-12 columns  small-order-2 medium-order-3" >
<?php while ( have_posts() ) : the_post(); ?>

                     <?php the_content();?>
<?php endwhile;?>
                     </div>

             </div>
        </div>
        <aside class="columns small-12 medium-3">
            <?php dynamic_sidebar('sidebar-widgets'); ?>
        </aside>
    </div>

<?php get_footer(); ?>